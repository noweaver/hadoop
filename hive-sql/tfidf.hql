-----------------------------------------------------------------------
drop table vcrm_6442251.self_intro_2013_by_doc
;
create table if not exists vcrm_6442251.self_intro_2013_by_doc 
as
select concat(year, "_", season, "_", channel, "_", id) as doc, concat(content1, " ", content2, " ", content3, " ", content4, " ", content5) as content from vcrm_6442251.self_intro4 where year = 2013
;

select concat(year, "_", season, "_", channel, "_", id) as doc, concat(content1, " ", content2, " ", content3, " ", content4, " ", content5) as content from vcrm_6442251.self_intro4 where year = 2013
;

select count(*) from terms_cnt_2013
;
-- 16619414

drop table vcrm_6442251.terms_cnt_2013
;
create table vcrm_6442251.terms_cnt_2013
as
select * from default.terms_cnt_2013
;

select count(*) from vcrm_6442251.terms_cnt_2013
;
-- 16619414


select doc_name, term, tf
from 
	vcrm_6442251.terms_cnt_2013
;

-- 문서별 총 키워드 수
select doc_name, sum(tf) all_keyword_cnt_per_doc
from (
	select doc_name, term, tf
	from 
		vcrm_6442251.terms_cnt_2013
	) t1
group by
	doc_name
order by
	doc_name
;

-- TF 값 
select
	t2.doc_name, t2.term, t2.tf as cnt, (t2.tf / t3.all_keyword_cnt_per_doc) as tf
from 
	vcrm_6442251.terms_cnt_2013 t2 join 
	(
		select doc_name, sum(tf) all_keyword_cnt_per_doc
		from vcrm_6442251.terms_cnt_2013		
		group by doc_name
	) t3 on (t2.doc_name = t3.doc_name) 
order by
	cnt, tf
;

-- 문서별 단어 수
select term, count(*) as term_doc_cnt
from 
	vcrm_6442251.terms_cnt_2013
group by
	term
;

-- idf 값
select t1.term, (1 + log10(59286/t2.term_doc_cnt)) as idf
from 
	vcrm_6442251.terms_cnt_2013 t1 join 
	(
		select term, count(*) as term_doc_cnt
		from 
			vcrm_6442251.terms_cnt_2013
		group by 
			term
	) t2 on (t1.term = t2.term)
;

-- 전체 문서 수
select count(distinct doc_name) as all_documents_cnt 
from vcrm_6442251.terms_cnt_2013
;


select
	t2.doc_name, t2.term, t2.tf as cnt, (t2.tf / t3.all_keyword_cnt_per_doc) as tf
from 
	vcrm_6442251.terms_cnt_2013 t2 join 
	(
		select doc_name, sum(tf) all_keyword_cnt_per_doc
		from (
			select doc_name, term, tf
			from 
				vcrm_6442251.terms_cnt_2013
		) t1
		group by
		doc_name
	) t3 on (t2.doc_name = t3.doc_name) 

-- tf, idf 
select 
	t1.doc_name, t1.term, t1.tf as cnt, (t1.tf / t2.all_keyword_cnt_per_doc) as tf, (1 + log10(t3.total_doc_cnt/t4.term_doc_cnt)) as idf
from 
	vcrm_6442251.terms_cnt_2013 t1 
	join 
	(  		
  		select doc_name, sum(tf) all_keyword_cnt_per_doc from vcrm_6442251.terms_cnt_2013		
		group by doc_name
	) t2 on t1.doc_name = t2.doc_name 
	join
	(
  		select count(distinct doc_name) as total_doc_cnt from vcrm_6442251.terms_cnt_2013
	) t3 on 1=1
	join
	(
  		select term, count(*) as term_doc_cnt from default.hrd_hyundai_cons_t_token
  		group by term
	) t4
  	on t1.term = t4.term

-- tf * idf
select
	t5.doc_name, t5.term, t5.cnt, t5.tf, t5.idf, (t5.tf * t5.idf) as tfidf
from 
	(
	select 
		t1.doc_name, t1.term, t1.tf as cnt, (t1.tf / t2.all_keyword_cnt_per_doc) as tf, (1 + log10(t3.total_doc_cnt/t4.term_doc_cnt)) as idf
	from 
		vcrm_6442251.terms_cnt_2013 t1 
		join 
		(  		
	  		select doc_name, sum(tf) all_keyword_cnt_per_doc from vcrm_6442251.terms_cnt_2013		
			group by doc_name
		) t2 on t1.doc_name = t2.doc_name 
		join
		(
	  		select count(distinct doc_name) as total_doc_cnt from vcrm_6442251.terms_cnt_2013
		) t3 on 1=1
		join
		(
	  		select term, count(*) as term_doc_cnt from default.hrd_hyundai_cons_t_token
	  		group by term
		) t4
	  	on t1.term = t4.term
	) t5
order by
	t5.doc_name


select doc_name, sum(tf) all_keyword_cnt_per_doc
from (
	select doc_name, term, tf
	from 
		vcrm_6442251.terms_cnt_2013
	) t1
where
	doc_name = '2013_first_regular_hmstreamlines2'
group by
	doc_name
order by
	doc_name
;
-- 432

select doc_name, term, tf, (tf/432) as normal_tf
from 
	vcrm_6442251.terms_cnt_2013
where
	doc_name = '2013_first_regular_hmstreamlines2'
order by
tf desc
;
      



select doc_name, count(*) from vcrm_6442251.terms_cnt_2013
group by doc_name
limit 100
;

-- 2013 전체 문서 수 
select
	count(*) as all_documents_cnt
from (
	select doc_name, count(*) from vcrm_6442251.terms_cnt_2013
	group by doc_name
	) t1
;
-- 59286

select count(distinct doc_name) as all_documents_cnt 
from vcrm_6442251.terms_cnt_2013
;
-- 59286