-- =======================================================================================================================================================================
-- VIN별 TF-IDF Score 구하기
-- 패턴 ID : Term
-- VIN : Doc
-- =======================================================================================================================================================================

drop table if exists vcrm_6442267.rg_log_um_f10_49_tfidf;
create table if not exists vcrm_6442267.rg_log_um_f10_49_tfidf
(
	doc_name 		string
	, term 			string
	, tfidf 			double
)
row format delimited fields terminated by '\t' 
lines terminated by '\n'
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

insert overwrite table vcrm_6442267.rg_log_um_f10_49_tfidf
if not exists
select t4.doc_name, t4.term, cast((t4.tf * t4.idf) as double) as tfidf
from 
(
	select t1.doc_name, t1.term, t1.tf as cnt
		, ((0.5 + (0.5 * t1.tf) / t2.max_tf_in_doc)) as tf 	
		, t3.idf as idf
	from 
		(
			select vin as doc_name, id as term, count(*) as tf from vcrm_6442267.rg_log_um_f10_factor
			group by vin, id
		) t1 
		join 
		(  	
			select vin as doc_name, max(tf) as max_tf_in_doc
			from
			(
				select vin, id, count(*) as tf from vcrm_6442267.rg_log_um_f10_factor
		  		group by vin, id
		  	) A
		  	group by vin
		) t2 on (t1.doc_name = t2.doc_name)
		join
		(
			select tt1.term, tt1.term_cnt_in_all_docs, (1 + log10(tt2.all_documents_cnt / tt1.term_cnt_in_all_docs)) as idf
			from 
			(
				select id as term, cnt as term_cnt_in_all_docs from vcrm_6442267.rg_log_um_pattern_f10_49
			) tt1 
			cross join 
			(
				select count(distinct vin) as all_documents_cnt 
				from vcrm_6442267.rg_log_um_f10_factor
			) tt2
		) t3 on (t1.term = t3.term)
) t4;
;

-- =======================================================================================================================================================================
-- 패턴ID별 TF-IDF Score 구하기
-- 패턴 ID - 1 : Term
-- 패턴 ID : Doc
-- =======================================================================================================================================================================

-- 패턴ID에서 Term 추출하기
-- 49개의 센서값으로 구성된 패턴ID에서 한자리씩 빼서 Term 만들기 

-- delete jar hdfs:///user/vcrm_6442267/udf/analysis.udf-0.0.1-SNAPSHOT.jar;
-- drop temporary function if exists termgenerator;
-- ADD JAR hdfs:///user/vcrm_6442267/udf/analysis.udf-0.0.1-SNAPSHOT.jar;

delete jar hdfs:///user/vcrm_master/lib/analysis.udf-0.0.1-SNAPSHOT.jar;
drop temporary function if exists termgenerator;

ADD JAR hdfs:///user/vcrm_master/lib/analysis.udf-0.0.1-SNAPSHOT.jar;
CREATE TEMPORARY FUNCTION termgenerator as 'org.apache.hadoop.hive.ql.udtf.TermGeneratorUDTF';

drop table if exists vcrm_6442267.rg_log_um_pattern_f10_49_term;
create table vcrm_6442267.rg_log_um_pattern_f10_49_term STORED AS PARQUET
as
SELECT   
 id
 ,ad.seq
 ,ad.term
FROM vcrm_6442267.rg_log_um_pattern_f10_49
lateral view termgenerator(id) ad as seq, term
;
-- 917179895

-- TF-IDF
drop table if exists vcrm_6442267.rg_log_um_pattern_f10_49_term_tfidf;
create table if not exists vcrm_6442267.rg_log_um_pattern_f10_49_term_tfidf
(
	doc_name 		string
	, term 			string
	, tfidf 			double
)
row format delimited fields terminated by '\t' 
lines terminated by '\n'
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

insert overwrite table vcrm_6442267.rg_log_um_pattern_f10_49_term_tfidf
if not exists
select t4.doc_name, t4.term, cast((t4.tf * t4.idf) as double) as tfidf
from 
(
	select t1.doc_name, t1.term, t1.tf as cnt
		, ((0.5 + (0.5 * t1.tf) / t2.max_tf_in_doc)) as tf 	
		, t3.idf as idf
	from 
		(
			select id as doc_name, term, count(*) as tf from rg_log_um_pattern_f10_49_term
			group by id, term
		) t1 
		join 
		(  	
			select id as doc_name, max(tf) as max_tf_in_doc
			from
			(
				select id, term, count(*) as tf from vcrm_6442267.rg_log_um_pattern_f10_49_term
		  		group by id, term
		  	) A
		  	group by id
		) t2 on (t1.doc_name = t2.doc_name)
		join
		(
			select tt1.term, tt1.term_cnt_in_all_docs, (1 + log10(tt2.all_documents_cnt / tt1.term_cnt_in_all_docs)) as idf
			from 
			(
				select term, count(*) as term_cnt_in_all_docs from vcrm_6442267.rg_log_um_pattern_f10_49_term
				group by term
			) tt1 
			cross join 
			(
				select count(distinct id) as all_documents_cnt 
				from vcrm_6442267.rg_log_um_pattern_f10_49_term
			) tt2
		) t3 on (t1.term = t3.term)
) t4;
;

-- =======================================================================================================================================================================
-- 패턴ID별 Cosine-Similarity 구하기
-- =======================================================================================================================================================================

-- 분자의 값만 따로 구하기 
-- 테이블 생성하고, cosine-similarity paring MR 실행
drop table if exists vcrm_6442267.rg_log_um_pattern_f10_49_numer;
create table if not exists vcrm_6442267.rg_log_um_pattern_f10_49_numer
(
	pair_docs		string	
	, numerator		 	double
)
row format delimited fields terminated by '\t' 
lines terminated by '\n'
-- stored as textfile
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

-- cosine-pairing job 실행
#!/usr/bin/env bash

#-Dmapreduce.map.memory.mb=32768 \
#-Dmapreduce.map.java.opts=-Xmx26214 \
#-Dmapreduce.reduce.memory.mb=32768 \
#-Dmapreduce.reduce.java.opts=-Xmx26214 \
#-Dyarn.app.mapreduce.am.resource.mb=32768 \
#-Dyarn.app.mapreduce.am.command-opts=-Xmx26214 \
#-Dmapreduce.task.timeout=86400000 \

#/user/hive/warehouse/vcrm_6442251.db/step4_2013_terms \
#/user/hive/warehouse/vcrm_6442251.db/step5_2013_terms

#mv ./snippet-hadooop-0.0.1-SNAPSHOT-job.jar ./paring-cosine-term-job.jar

hadoop jar \
./paring-cosine-term-job.jar \
similarity.cosine.mr.term.parquet.ParingDocsTfidf \
-Dmapreduce.job.reduces=100 \
-Dmapreduce.map.output.compress=true \
-Dmapreduce.output.fileoutputformat.compress.type=BLOCK \
-Dmapreduce.map.output.compress.codec=org.apache.hadoop.io.compress.SnappyCodec \
-Dmapreduce.input.fileinputformat.split.minsize=268435456 \
/user/hive/warehouse/vcrm_6442267.db/rg_log_um_pattern_f10_49_term_tfidf \
/user/hive/warehouse/vcrm_6442267.db/rg_log_um_pattern_f10_49_numer


-- 분모의 값만 따로 구하기 
drop table if exists vcrm_6442267.rg_log_um_pattern_f10_49_denom;
create table if not exists vcrm_6442267.rg_log_um_pattern_f10_49_denom
(
	doc_name 		string
	, denominator 	double
)
row format delimited fields terminated by '\t' 
lines terminated by '\n'
stored as textfile
-- stored as parquet
tblproperties ("skip.header.line.count" = "1")
;


insert overwrite table vcrm_6442267.rg_log_um_pattern_f10_49_denom
if not exists
select doc_name, sqrt(sum(tfidf * tfidf)) as denominator
from vcrm_6442267.rg_log_um_pattern_f10_49_term_tfidf
group by doc_name
;

--  테이블 생성하고, cosine-similarity MR 실행
drop table if exists vcrm_6442267.rg_log_um_pattern_f10_49_cosine;
create table if not exists vcrm_6442267.rg_log_um_pattern_f10_49_cosine
(
	doc_name1			string	
	, doc_name2		 	string
	, similarity		double
)
row format delimited fields terminated by '\t' 
lines terminated by '\n'
-- stored as textfile
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

-- 분산캐시를 위한 파일 병합(디멘젼 테이블)
hadoop fs -getmerge /user/hive/warehouse/vcrm_6442267.db/rg_log_um_pattern_f10_49_denom/ ./denom.txt
hadoop fs -put ./denom.txt /user/vcrm_6442267/dimension

-- cosine-similarity job 실행
#!/usr/bin/env bash

#-Dmapreduce.map.memory.mb=32768 \
#-Dmapreduce.map.java.opts=-Xmx26214 \
#-Dmapreduce.reduce.memory.mb=32768 \
#-Dmapreduce.reduce.java.opts=-Xmx26214 \
#-Dyarn.app.mapreduce.am.resource.mb=32768 \
#-Dyarn.app.mapreduce.am.command-opts=-Xmx26214 \
#-Dmapreduce.task.timeout=86400000 \

#/user/hive/warehouse/vcrm_6442267.db/rg_log_um_pattern_f10_49_denom/000000_0 \

mv ./snippet-hadooop-0.0.1-SNAPSHOT-job.jar ./cosine-similarity-term-job.jar

hadoop jar \
./cosine-similarity-term-job.jar \
similarity.cosine.mr.term.parquet.CosineSimilarity \
-Dmapreduce.map.output.compress=true \
-Dmapreduce.output.fileoutputformat.compress.type=BLOCK \
-Dmapreduce.map.output.compress.codec=org.apache.hadoop.io.compress.SnappyCodec \
-Dmapreduce.input.fileinputformat.split.minsize=268435456 \
-Dmapreduce.job.reduces=100 \
/user/hive/warehouse/vcrm_6442267.db/rg_log_um_pattern_f10_49_numer \
/user/vcrm_6442267/dimension/denom.txt \
/user/hive/warehouse/vcrm_6442267.db/rg_log_um_pattern_f10_49_cosine \
parquet

-- ===================================================================================================================================================================
