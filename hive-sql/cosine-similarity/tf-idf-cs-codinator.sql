-- =========================================================================================================================================================
-- Pre processing
-- =========================================================================================================================================================
drop table if exists vcrm_6442251_tm.00_extract_2015_self_intro;
create table if not exists vcrm_6442251_tm.00_extract_2015_self_intro stored as parquet
as
select
	cast(hgrcode as int)
	, empgubun
	, year
	, kisu
	, userid
	, ci
	, case when gender = "1" then "m" when gender = "f" then "f" else "none" end as gender
	, lhodepnm as apply_part
	, lhowornm as apply_sub_part
	, introduction
	, motive
	, liferesult
	, clobact
from
	vcrm_6442251_tm.2015_self_intro2
where
	year = '2015'
	and (kisu = '32' or kisu = '83' or kisu = '84' or kisu = '85' or kisu = '86' or kisu = '16')
	-- and (length(introduction) >= 500 and length(motive) >= 500 and length(liferesult) >= 500 )
;

select count(*) from 00_extract_2015_self_intro
;
-- 39,905


-- =========================================================================================================================================================
-- 1단계 : 로데이터 정리
-- year, channel, season, id 합치기
-- 모든 답변 합치기
-- 총 질문은 3개, 각 질문 당 1,000자 작성되어 있음. 각 답변당 500자 미만은 스킵(성의 문제로 읽지 않음. 실제로 내용도 부족)
-- 소요시간 : 3분
-- =========================================================================================================================================================
drop table if exists vcrm_6442251_tm.01_2015_self_intro
;
create table if not exists vcrm_6442251_tm.01_2015_self_intro
(
    doc_name 		string
    , ci                		string
    , gender 		string
    , apply_part        	string
    , apply_sub_part    	string
    , length            	int
    , blank             	int
    , content           	string
)
row format delimited fields terminated by '\t'
lines terminated by '\n'
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

delete jar hdfs:///user/vcrm_6442251/udf/extract_korean-v4.jar;
delete jar hdfs:///user/vcrm_6442251/udf/extract_korean-v5.jar;
delete jar hdfs:///user/vcrm_6442251/udf/extract_korean-v6.jar;

drop temporary function if exists extract_korean;

add file hdfs:///user/vcrm_6442251/udf/extract_korean-v6.jar;
add jar hdfs:///user/vcrm_6442251/udf/extract_korean-v6.jar;

create temporary function extract_korean as 'hive.udf.ExtractKorean';

insert overwrite table vcrm_6442251_tm.01_2015_self_intro
if not exists
select
    t1.doc_name
    , t1.ci
    , t1.gender
    , t1.apply_part
    , t1.apply_sub_part
    , t1.length
    , t1.length - (length(regexp_replace(t1.content, " ", ""))) as blank
    , t1.content
from (
    select
        concat(hgrcode, "_", substr(year, 3), "_", empgubun, "_",  kisu, "_", userid) as doc_name
        , ci
        , gender
        , apply_part
        , apply_sub_part
        , length(trim(extract_korean(concat(introduction, " ", motive, " ", liferesult, " ", clobact)))) as length
        , trim(extract_korean(concat(introduction, " ", motive, " ", liferesult, " ", clobact))) as content
    from
        vcrm_6442251_tm.00_extract_2015_self_intro
    where
        length(introduction) >= 500
        and length(motive) >= 500
        and length(liferesult) >= 500
) t1
;

select count(distinct doc) from vcrm_6442251_tm.01_2015_self_intro
;
-- 27321

-- =========================================================================================================================================================
-- 3단계 : 워크플로우를 통해서 문서 토큰나이즈
-- 소요시간: 40분
-- =========================================================================================================================================================
select  doc_name as doc, content from vcrm_6442251_tm.01_2015_self_intro
;

drop table if exists vcrm_6442251_tm.02_2015_terms;
create table if not exists vcrm_6442251_tm.02_2015_terms stored as parquet
as
select * from default.02_terms_2015
;

select count(distinct doc_name) from vcrm_6442251_tm.02_2015_terms
;
-- 27317

-- =========================================================================================================================================================
-- 3단계 : TF/IDF 구하기
-- 소요시간: 1분
-- =========================================================================================================================================================
drop table if exists vcrm_6442251_tm.03_2015_terms_tfidf;
create table if not exists vcrm_6442251_tm.03_2015_terms_tfidf
(
	doc_name 		string
	, term 			string
	, tfidf 			double
)
row format delimited fields terminated by '\t'
lines terminated by '\n'
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

insert overwrite table vcrm_6442251_tm.03_2015_terms_tfidf
if not exists
select t4.doc_name, t4.term, cast((t4.tf * t4.idf) as double) as tfidf
from
(
	select t1.doc_name, t1.term, t1.tf as cnt
		, ((0.5 + (0.5 * t1.tf) / t2.max_tf_in_doc)) as tf
		, t3.idf as idf
	from
		vcrm_6442251_tm.02_2015_terms t1
		join
		(
			select doc_name, max(tf) as max_tf_in_doc from vcrm_6442251_tm.02_2015_terms
	  		group by doc_name
		) t2 on (t1.doc_name = t2.doc_name)
		join
		(
			select t1.term, t1.term_cnt_in_all_docs, (1 + log10(t2.all_documents_cnt / t1.term_cnt_in_all_docs)) as idf
			from
			(
				select term, count(*) as term_cnt_in_all_docs from vcrm_6442251_tm.02_2015_terms
				group by term
			) t1
			cross join
			(
				select count(distinct doc_name) as all_documents_cnt
				from vcrm_6442251_tm.02_2015_terms
			) t2
		) t3 on (t1.term = t3.term)
) t4
;

select count(distinct doc_name) from vcrm_6442251_tm.03_2015_terms_tfidf
;
-- 27317

-- =========================================================================================================================================================
-- 4단계 : 테이블 생성하고, cosine-similarity paring MR 실행
-- 분자의 값만 따로 구하기
-- 소요시간: 1시간 20분 / 2015 => 14mins, 31sec
-- =========================================================================================================================================================
drop table if exists vcrm_6442251_tm.04_2015_docs_paring;
create table if not exists vcrm_6442251_tm.04_2015_docs_paring
(
	pair_docs		string
	, numerator		double
)
row format delimited fields terminated by '\t'
lines terminated by '\n'
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

select count(distinct t1.doc_name1)
from
(
	select split(pair_docs, ":")[0] as doc_name1 from vcrm_6442251_tm.04_2015_docs_paring
) t1
;
-- 27315

select count(distinct t1.doc_name2)
from
(
	select split(pair_docs, ":")[1] as doc_name2 from vcrm_6442251_tm.04_2015_docs_paring
) t1
;
-- 27315

-- =========================================================================================================================================================
-- 5단계 : 분자의 값만 따로 구하기
-- distirbuted cached 될 것이므로 parquet이 아닌 textfile로 저장할 것
-- 소요시간: 35초
-- =========================================================================================================================================================
drop table if exists vcrm_6442251_tm.05_2015_terms_denom;
create table if not exists vcrm_6442251_tm.05_2015_terms_denom
(
	doc_name 		string
	, denominator 		double
)
row format delimited fields terminated by '\t'
lines terminated by '\n'
stored as textfile
tblproperties ("skip.header.line.count" = "1")
;

insert overwrite table vcrm_6442251_tm.05_2015_terms_denom
if not exists
select doc_name, sqrt(sum(tfidf * tfidf)) as denominator
from vcrm_6442251_tm.03_2015_terms_tfidf
group by doc_name
;

select count(distinct doc_name1) from 05_2015_terms_denom
;
-- 27316

-- =========================================================================================================================================================
-- 6단계 : 테이블 생성하고, cosine-similarity MR 실행
-- 소요시간: 6시간 20분 ==> 3hrs, 19mins, 1sec
-- =========================================================================================================================================================
drop table if exists vcrm_6442251_tm.06_2015_similarity;
create table if not exists vcrm_6442251_tm.06_2015_similarity
(
	doc_name1			string
	, doc_name2		 	string
	, similarity			double
)
row format delimited fields terminated by '\t'
lines terminated by '\n'
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;


-- =========================================================================================================================================================
-- 7단계: 문장 추출
-- =========================================================================================================================================================
delete jar hdfs:///user/vcrm_6442251/udf/sentenizer-v5.jar;
drop temporary function if exists sentenizer ;

add file hdfs:///user/vcrm_6442251/udf/sentenizer-v5.jar;
add jar hdfs:///user/vcrm_6442251/udf/sentenizer-v5.jar;

create temporary function sentenizer as 'hive.udtf.Sentenizer' ;

drop table if exists vcrm_6442251_tm.07_2015_sentenizer;
create table if not exists vcrm_6442251_tm.07_2015_sentenizer stored as parquet
as
select
    doc_name,
    cnt,
    trim(regexp_replace(regexp_replace(sentence, "[\',;:\\/()<>\\[\\]{}ㅡ|?!.]+", ""), "[ㅏㅑㅓㅕㅗㅛㅜㅠㅡㅣㄱㄲㄴㄷㄸㄹㅁㅂㅃㅅㅆㅇㅈㅉㅊㅋㅌㅍㅎ]",     "")) as sentence
from
(
    select sentenizer(doc, content) as (doc_name, sentence, cnt)
    from vcrm_6442251_tm.01_2015_self_intro
) t1
where
    length(t1.sentence) > 0
;

-- =========================================================================================================================================================
-- 8단계 : Sentence TF-IDF
-- =========================================================================================================================================================
drop table if exists vcrm_6442251_tm.08_2015_sentence_tfidf;
create table if not exists vcrm_6442251_tm.08_2015_sentence_tfidf
(
	doc_name 		string
	, sentence		string
	, tfidf 			double
)
row format delimited fields terminated by '\t'
lines terminated by '\n'
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

insert overwrite table vcrm_6442251.08_2015_sentence_tfidf
if not exists
select t4.doc_name, t4.sentence, (t4.tf * t4.idf) as tfidf
from
(
	select t1.doc_name, t1.sentence, t1.cnt as cnt
		, ((0.5 + (0.5 * t1.cnt) / t2.max_tf_in_doc)) as tf
		, t3.idf as idf
	from
		vcrm_6442251_tm.07_2015_sentenizer t1
		join
		(
			select doc_name, max(cnt) as max_tf_in_doc from vcrm_6442251_tm.07_2015_sentenizer
	  		group by doc_name
		) t2 on (t1.doc_name = t2.doc_name)
		join
		(
			select t1.sentence, t1.sen_cnt_in_all_docs, (1 + log10(t2.all_documents_cnt / t1.sen_cnt_in_all_docs)) as idf
			from
			(
				select sentence, count(*) as sen_cnt_in_all_docs from vcrm_6442251_tm.07_2015_sentenizer
				group by sentence
			) t1
			cross join
			(
				select count(distinct doc_name) as all_documents_cnt
				from vcrm_6442251_tm.07_2015_sentenizer
			) t2
		) t3 on (t1.sentence = t3.sentence)
) t4
;

-- =========================================================================================================================================================
-- 9단계:
-- Sentence 단위 Pairing MR 실행하고 Table 생성
-- 소요시간:

drop table if exists vcrm_6442251_tm.09_2015_same_sentence_by_id;
create table if not exists vcrm_6442251_tm.09_2015_same_sentence_by_id
(
	pair_docs			string
	, numerator 			double
	, sentence			string
)
row format delimited fields terminated by '\t'
lines terminated by '\n'
stored as parquet
tblproperties ("skip.header.line.count" = "1")
;

select sentence, count(*), pair_docs
from vcrm_6442251_tm.09_2015_same_sentence_by_id
group by sentence, pair_docs



-- =========================================================================================================================================================
-- 8단계 : After paring by id, Extact sentence
-- Sentence 단위 Pairing MR 실행하고 Table 생성
-- 소요시간:
-- =========================================================================================================================================================
-- drop table if exists vcrm_6442251_tm.07_same_sentence_by_id;
-- create table if not exists vcrm_6442251_tm.07_same_sentence_by_id stored as parquet
-- as
-- select t1.pair_docs as doc_name, t2.cnt, t1.sentence
-- from
-- 	vcrm_6442251_tm.09_2015_same_sentence_by_id t1
-- 	join
-- 	(
-- 		select t1.pair_docs as pair_docs, t1.cnt as cnt
-- 		from
-- 		(
-- 			select pair_docs, count(*) as cnt
-- 			from vcrm_6442251_tm.09_2015_same_sentence_by_id
-- 			group by pair_docs
-- 		) t1
-- 		where
-- 			t1.cnt > 0
-- 	) t2
-- 	on (t1.pair_docs = t2.pair_docs)
-- where
-- 	length(t1.sentence)  > 20
-- 	and split(split(t1.pair_docs, ":")[0], "_")[1] != split(split(t1.pair_docs, ":")[1], "_")[1]
-- ;

drop table if exists vcrm_6442251_tm.10_2015_similarity_range;
create table if not exists vcrm_6442251_tm.10_2015_similarity_range stored as parquet
as
select doc_name1, doc_name2, similarity, percent
	, case when (percent >=0 and percent < 10) then 0
		when (percent >=10 and percent < 20) then 10
		when (percent >=20 and percent < 30) then 20
		when (percent >=30 and percent < 40) then 30
		when (percent >=40 and percent < 50) then 40
		when (percent >=50 and percent < 60) then 50
		when (percent >=60 and percent < 70) then 60
		when (percent >=70 and percent < 80) then 70
		when (percent >=80 and percent < 90) then 80
		when (percent >=90) then 90
	end as section
from
(
	select doc_name1, doc_name2, similarity, cast(similarity * 100 as int) as percent
	from vcrm_6442251_tm.06_2015_similarity
) t1
;

drop table if exists vcrm_6442251_tm.10_2015_similarity_range_20pcnt_over_report;
create table if not exists vcrm_6442251_tm.10_2015_similarity_range_20pcnt_over_report stored as parquet
as
select
	doc_name1
	, doc_name2
	, similarity
	, percent
	, section
from 10_2015_similarity_range
where
	percent >= 20
	and split(doc_name1, "_")[4]  != split(doc_name2, "_")[4]
;

select count(*) from vcrm_6442251_tm.10_2015_similarity_range_20pcnt_over
;
-- 24680

drop table if exists 10_2015_similarity_range_20pcnt_over2;
create table if not exists 10_2015_similarity_range_20pcnt_over2
as
select * from 10_2015_similarity_range_20pcnt_over
where pair_docs != '1_15_1_32_hmjolrim4u:1_15_2_16_hmgi4548'
order by percent desc
;

select count(*) from 10_2015_similarity_range_20pcnt_over2
;
-- 24679

select count(*) from vcrm_6442251_tm.09_2015_same_sentence_by_id
;
-- 524464

select t3.pair_docs,
from
(
	select
		t1.pair_docs
		, t1.similarity
		, t1.percent
		, t2.sentence
	from
	(
		select
			concat(doc_name1, ":", doc_name2) as pair_docs
			, similarity
			, percent
			, section
		from vcrm_6442251_tm.10_2015_similarity_range
		where section = 0 or section  = 10
	) t1
	inner join
	vcrm_6442251_tm.09_2015_same_sentence_by_id t2
	on
	(t1.pair_docs = t2.pair_docs)
	where
		length(t2.sentence) > 20
) t3
;


select
	t2.pair_docs as pair_docs, count(*) as cnt
from
	vcrm_6442251_tm.10_2015_similarity_range t1
	, vcrm_6442251_tm.09_2015_same_sentence_by_id t2
where
	(t1.section = 0 or t1.section  = 10)
	and length(t2.sentence) > 20
group by
	t2.pair_docs




select t4.*, t3.cnt as cnt
from
	(
		select
			t2.pair_docs, count(*) as cnt
		from
			(
				select *
				from vcrm_6442251_tm.10_2015_similarity_range
				where
					section = 0 or section = 10
			) t1
			, vcrm_6442251_tm.09_2015_same_sentence_by_id t2
		where
			concat(t1.doc_name1, ":", t1.doc_name2) = t2.pair_docs
		group by
			t2.pair_docs
	) t3
	, vcrm_6442251_tm.10_2015_similarity_range t4
order by cnt desc
;


select
	t2.pair_docs, count(*) as cnt -- t2.pair_docs, count(*) as cnt
from
	(
		select *
		from vcrm_6442251_tm.10_2015_similarity_range
		where
			section = 0 or section = 10
	) t1
	inner join
    vcrm_6442251_tm.09_2015_same_sentence_by_id t2
    on (concat(t1.doc_name1, ":", t1.doc_name2) = t2.pair_docs)
where
    length(sentence) > 20
group by
	t2.pair_docs
;




















drop table if exists vcrm_6442251_tm.10_2015_similarity_range_20pcnt_over2;
create table if not exists vcrm_6442251_tm.10_2015_similarity_range_20pcnt_over2 stored as parquet
as
select t1.*, t4.cnt as cnt
from
	10_2015_similarity_range_20pcnt_over t1
	, (
		select t3.pair_docs as pair_docs, count(*) as cnt
		from
			10_2015_similarity_range_20pcnt_over t2
			, 09_2015_same_sentence_by_id t3
		where
			concat(t2.doc_name1, ":", t2.doc_name2) = t3.pair_docs
		group by t3.pair_docs
	) t4
where
	concat(t1.doc_name1, ":", t1.doc_name2) = t4.pair_docs
order by cnt desc
;


-- =========================================================================================================================================================
-- 성별, 학교, 소재 정보
-- =========================================================================================================================================================

drop table if exists school_region;
create table if not exists school_region stored as parquet
as
select
	concat(cast(hgrcode as int), "_", substr(year, 3), "_", empgubun, "_",  kisu, "_", userid) as doc_name
	, acagubun
	, schoolnm
	, substr(loc_citynm, 0, 2) as region
from
	vcrm_6442251_tm.2015_academic2
where
	year = '2015'
	and (kisu = '32' or kisu = '83' or kisu = '84' or kisu = '85' or kisu = '86' or kisu = '16')
;
-- null = 72
--

drop table if exists school_region2;
create table if not exists school_region2 stored as parquet
as
select A.doc_name, A.schoolnm, B.acagubun
from (
select t2.doc_name, max(t2.schoolnm) as schoolnm
from
	school_region t2
	inner join
	(
		select doc_name, max(acagubun) as acagubun
		from school_region
		group by doc_name
	) t3
	on (t2.doc_name = t3.doc_name)
where
	t2.acagubun = t3.acagubun
group by t2.doc_name
) A
, (
select t2.doc_name, max(t2.acagubun) as acagubun
from
	school_region t2
	inner join
	(
		select doc_name, max(acagubun) as acagubun
		from school_region
		group by doc_name
	) t3
	on (t2.doc_name = t3.doc_name)
where
	t2.acagubun = t3.acagubun
group by t2.doc_name
) B
where A.doc_name = B.doc_name
;

select count(*)
from school_region2
;
-- 62120

select
	count(*)
	-- doc_name
    	-- , ci
    	-- , gender
    	-- , apply_part
    	-- , apply_sub_part
    	-- , length
    	-- , blank
    	-- , content
from
	01_2015_self_intro t1
	inner join
	school_region2 t2
	on (t1.doc_name = t2.doc_name)
;
-- 27321

drop table if exists vcrm_6442251_tm.01_2015_self_intro2;
create table if not exists vcrm_6442251_tm.01_2015_self_intro2 stored as parquet
as
select
	t1.doc_name
    	, t1.ci
    	, t1.gender
    	, t2.schoolnm
    	, t2.acagubun
    	, t1.apply_part
    	, t1.apply_sub_part
    	, t1.length
    	, t1.blank
    	, t1.content
from
	01_2015_self_intro t1
	inner join
	school_region2 t2
	on (t1.doc_name = t2.doc_name)
;


select count(*) from 00_extract_2015_self_intro
;
-- 39,905

select
	coun(*)
from 00_extract_2015_self_intro
where
        length(introduction) >= 500
        and length(motive) >= 500
        and length(liferesult) >= 500
;
-- 27321

select
	count(*)
from 00_extract_2015_self_intro
where
        length(introduction) < 500
        or length(motive) < 500
        or length(liferesult) < 500
-- 12584