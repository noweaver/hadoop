package tfidf2.step3;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WordsInCorpusTFIDF extends Configured implements Tool {
	// where to put the data in hdfs when we're done
	private static final String OUTPUT_PATH = "/Volumes/Workspace/Git/snippet-hadoop/data/result";

	// where to read the data from.
	private static final String INPUT_PATH = "/Volumes/Workspace/Git/snippet-hadoop/data/tfidf2_step2";

	public int run(String[] args) throws Exception {

		Configuration conf = getConf();
		Job job = Job.getInstance(conf, "Word in Corpus, TF-IDF");

		job.setJarByClass(WordsInCorpusTFIDF.class);
		job.setMapperClass(WordsInCorpusTFIDFMapper.class);
		job.setReducerClass(WordsInCorpusTFIDFReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(INPUT_PATH));
		FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH));

		// Getting the number of documents from the original input directory.
//		Path inputPath = new Path("input");
		Path inputPath = new Path("/Volumes/Workspace/Git/snippet-hadoop/data/input");
		FileSystem fs = inputPath.getFileSystem(conf);
		FileStatus[] stat = fs.listStatus(inputPath);

		// Dirty hack to pass the total number of documents as the job name.
		// The call to context.getConfiguration.get("docsInCorpus") returns null when I tried to pass
		// conf.set("docsInCorpus", String.valueOf(stat.length)) Or even
		// conf.setInt("docsInCorpus", stat.length)
		job.setJobName(String.valueOf(stat.length));

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new WordsInCorpusTFIDF(), args);
		System.exit(res);
	}
}
