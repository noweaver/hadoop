package standard;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimilarityDriver extends Configured implements Tool {
	private final Logger logger = LoggerFactory.getLogger(SimilarityDriver.class);
	private final Long SIZE_128MB = FileUtils.ONE_MB * 128;

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new SimilarityDriver(), args);
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws IOException {
		if (args.length != 2) {
			String msg = "Usage: hadoop jar snippet-hadop-0.0.1-SNAPSHOT-job.jar" + " [generic options] input output";
			System.err.println(msg);
			System.err.println();
			GenericOptionsParser.printGenericCommandUsage(System.err);

			return 1;
		}

		String HDFS_INPUT_PATH = args[0];
		String HDFS_OUTPUT_PATH = args[1];
		Configuration conf = getConf();
		conf.set("mapred.child.java.opts", "-Xmx4096m");

		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(HDFS_OUTPUT_PATH), true);

		Path inFile = new Path(HDFS_INPUT_PATH);
		// Path outFile = new Path(HDFS_OUTPUT_PATH);
		if (!fs.exists(inFile)) {
			System.out.println("File Not Found");
		}

		BufferedWriter bw = null;
		BufferedReader br = null;

		try {
			br = new BufferedReader(new InputStreamReader(fs.open(inFile)));
			String line = null;

			List<Document> docTermTfidfs = new ArrayList<Document>();

			int lineNumber = 1;
			while ((line = br.readLine()) != null) {
				int pos = line.indexOf("\t");
				String docName = line.substring(0, pos);
				pos += 1;
				String termTfidfs = line.substring(pos, line.length());

				List<String> termTfidfList = new ArrayList<String>();
				int index = 0;
				int end = 0;
				while ((end = termTfidfs.indexOf(",", index)) > 0) {
					termTfidfList.add(termTfidfs.substring(index, end));
					index = end + 1;
				}

				termTfidfList.add(termTfidfs.substring(index, termTfidfs.length()));
				termTfidfs = null;

				List<String> terms = new ArrayList<String>();
				List<String> tfidfs = new ArrayList<String>();
				for (String termTfidf : termTfidfList) {
					index = 0;
					end = 0;
					while ((end = termTfidf.indexOf("=", index)) > 0) {
						terms.add(termTfidf.substring(index, end));
						index = end + 1;
						tfidfs.add(termTfidf.substring(index, termTfidf.length()));
					}
				}

				String[] termArray = terms.toArray(new String[terms.size()]);
				String[] tfidfArray = tfidfs.toArray(new String[tfidfs.size()]);

				Document document = new Document(docName, termArray, tfidfArray);
				docTermTfidfs.add(document);

				logger.debug("=>" + lineNumber++);
			}
			int fileIndex = 1;
			StringBuilder twoDocs = new StringBuilder(512);
			StringBuilder fileName = new StringBuilder(15);
			logger.debug("Document size:" + docTermTfidfs.size());
			int doc1Index = 1;
			int doc2Index = 1;

			for (Document doc1 : docTermTfidfs) {
				String doc1Name = doc1.getName();

				for (Document doc2 : docTermTfidfs) {
					String doc2Name = doc2.getName();
					if (doc1Name.equals(doc2Name)) {
						continue;
					}

					String[] twoTerms = twoTerms(doc1.getTerms(), doc2.getTerms());
					StringBuilder vector1 = new StringBuilder(256);
					StringBuilder vector2 = new StringBuilder(256);

					for (int i = 0, size = twoTerms.length; i < size; i++) {
						vector1.append(doc1.contain(twoTerms[i])).append(",");
						vector2.append(doc2.contain(twoTerms[i])).append(",");
					}

					twoDocs.append(doc1Name).append("@").append(doc2Name).append("\t");
					twoDocs.append(vector1.toString().substring(0, (vector1.length() - 1))).append("\n");
					twoDocs.append(doc1Name).append("@").append(doc2Name).append("\t");
					twoDocs.append(vector2.toString().substring(0, (vector2.length() - 1)));

					if (twoDocs.length() > SIZE_128MB) {
						fileName.append(HDFS_OUTPUT_PATH).append("_").append(fileIndex++).append(".text");

						bw = new BufferedWriter(new OutputStreamWriter(fs.create(new Path(fileName.toString()))));
						bw.write(twoDocs.toString());
						bw.flush();

						twoDocs.setLength(0);
						fileName.setLength(0);
					}
					logger.debug(doc1Index++ + "/" + doc2Index++);
				}

				if (twoDocs.length() > SIZE_128MB) {
					fileName.append(HDFS_OUTPUT_PATH).append("_").append(fileIndex++).append(".text");

					bw = new BufferedWriter(new OutputStreamWriter(fs.create(new Path(fileName.toString()))));
					bw.write(twoDocs.toString());
					bw.flush();

					twoDocs.setLength(0);
					fileName.setLength(0);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			bw.close();
			br.close();
		}

		return 0;
	}

	private String[] twoTerms(String[] terms1, String[] terms2) {
		Set<String> termSet = new HashSet<String>();
		termSet.addAll(Arrays.asList(terms1));
		termSet.addAll(Arrays.asList(terms2));

		String[] twoTerms = termSet.toArray(new String[termSet.size()]);
		return twoTerms;
	}
}
