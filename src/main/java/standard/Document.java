package standard;

public class Document {
	private String docName;
	private String[] terms;
	private String[] tfidfs;

	public Document(String docName, String[] termArray, String[] tfidfArray) {
		this.docName = docName;
		this.terms = termArray;
		this.tfidfs = tfidfArray;
	}

	public String getName() {
		return this.docName;
	}

	public String[] getTerms() {
		return terms;
	}

	public String[] getTfidfs() {
		return tfidfs;
	}

	@Override
	public String toString() {
		return "Document=" + docName + "]";
	}

	public String contain(String term) {
		for (int i = 0, size = terms.length; i < size; i++) {
			if (term.equals(terms[i])) {
//				String tfidf = tfidfs[i];
//				int pos = tfidf.indexOf(".");
//				tfidf = tfidf.substring(0, pos + 6);
//				return tfidf;
				return tfidfs[i];
			}
		}
		return "0.0";
	}
}
