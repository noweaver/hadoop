package standard;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimilarityDriverTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testMain() throws Exception {
		String inputPath = "D:/Workspace/Git/snippet-hadoop/data/input/result.text";
		String outputPath = "D:/Workspace/Git/snippet-hadoop/data/ouput/result.text";

		String[] args = { inputPath, outputPath };
		SimilarityDriver.main(args);

	}

	@Test
	public void testSplit1() throws Exception {
		String line = "2013_first_H-Innovator_hm1212gusrnr\t[\"동차=1.605558431937124\",\"강화=1.17873628616625\",\"저희=0.7769741342151326\"]";
		Map<String, Map<String, Double>> docMap = new HashMap<String, Map<String, Double>>();

		int pos = line.indexOf("\t");
		String docName = line.substring(0, pos);
		pos += 1;
		String tfidfs = line.substring(pos, line.length());

		pos = tfidfs.indexOf("[");
		int endPos = tfidfs.lastIndexOf("]");

		tfidfs = tfidfs.substring(pos + 1, endPos);

		List<String> tfidfList = new ArrayList<String>();
		int index = 0;
		while ((index = tfidfs.indexOf(",")) > 0) {
			tfidfList.add(tfidfs.substring(0, index));
			tfidfs = tfidfs.substring(index + 1, tfidfs.length());
		}
		tfidfList.add(tfidfs);

		Map<String, Double> keyValue = new HashMap<String, Double>();
		for (String termTfidf : tfidfList) {
			pos = termTfidf.indexOf("\"");
			endPos = termTfidf.lastIndexOf("\"");
			termTfidf = termTfidf.substring(pos + 1, endPos);

			pos = termTfidf.indexOf("=");
			String key = termTfidf.substring(0, pos);
			double value = Double.parseDouble(termTfidf.substring(pos + 1, termTfidf.length()));

			keyValue.put(key, value);
		}
		docMap.put(docName, keyValue);

		StringBuilder sb = new StringBuilder();
		for (String doc1 : docMap.keySet()) {
			Map<String, Double> similarDocs = new HashMap<String, Double>();

			for (String doc2 : docMap.keySet()) {
				// if (doc1.equals(doc2)) {
				// continue;
				// }

				Map<String, Double> doc1Map = docMap.get(doc1);
				Set<String> doc1Terms = doc1Map.keySet();

				Map<String, Double> doc2Map = docMap.get(doc1);
				Set<String> doc2Terms = doc2Map.keySet();

				Set<String> allTerms = new TreeSet<String>();
				allTerms.addAll(doc1Terms);
				allTerms.addAll(doc2Terms);

				int size = allTerms.size();
				double[] doc1Vector = new double[size];
				double[] doc2Vector = new double[size];

				// vectorizing
				int i = 0, j = 0;
				Iterator<String> it = allTerms.iterator();
				while (it.hasNext()) {
					String term = it.next();

					if (doc1Map.containsKey(term)) {
						doc1Vector[i++] = doc1Map.get(term);
					} else {
						doc1Vector[i++] = 0;
					}

					if (doc2Map.containsKey(term)) {
						doc2Vector[j++] = doc2Map.get(term);
					} else {
						doc2Vector[j++] = 0;
					}
				}
			}
		}
	}

	private final Logger logger = LoggerFactory.getLogger(SimilarityDriverTest.class);

	@Test
	public void test() throws IOException {
		BufferedReader br = null;

		try {
			// String in = "D:/Workspace/Git/snippet-hadoop/data/input/result.text";
			String in = "D:/Workspace/Git/snippet-hadoop/data/input/000000_0";
			String out = "D:/Workspace/Git/snippet-hadoop/data/output/output.text";

			deleteFile(out);

			br = new BufferedReader(new FileReader(in));

			List<Document> docTermTfidfs = new ArrayList<Document>();
			String line = null;
			int lineNumber = 1;
			while ((line = br.readLine()) != null) {
				int pos = line.indexOf("\t");
				String docName = line.substring(0, pos);
				pos += 1;
				String termTfidfs = line.substring(pos, line.length());

				List<String> termTfidfList = new ArrayList<String>();
				int index = 0;
				int end = 0;
				while ((end = termTfidfs.indexOf(",", index)) > 0) {
					termTfidfList.add(termTfidfs.substring(index, end - 10));
					index = end + 1;
				}

				termTfidfList.add(termTfidfs.substring(index, termTfidfs.length() - 10));
				termTfidfs = null;

				List<String> terms = new ArrayList<String>();
				List<String> tfidfs = new ArrayList<String>();
				for (String termTfidf : termTfidfList) {
					StringTokenizer st = new StringTokenizer(termTfidf, "=");
					String term = st.nextToken();
					String tfidf = st.nextToken();

					// pos = termTfidf.indexOf("=");
					// String term = termTfidf.substring(0, pos);
					// String tfidf = termTfidf.substring(pos + 1, termTfidf.length());
					logger.info("(" + lineNumber + ") " + docName + "=>" + term + ":" + tfidf);
					terms.add(term);
					tfidfs.add(tfidf);
				}

				String[] termArray = terms.toArray(new String[terms.size()]);
				String[] tfidfArray = tfidfs.toArray(new String[tfidfs.size()]);

				Document document = new Document(docName, termArray, tfidfArray);
				docTermTfidfs.add(document);
				lineNumber++;
			}

			int parsingLine = 1;
			StringBuilder twoDocs = new StringBuilder(512);
			for (Document doc1 : docTermTfidfs) {
				String doc1Name = doc1.getName();

				for (Document doc2 : docTermTfidfs) {
					String doc2Name = doc2.getName();
					if (doc1Name.equals(doc2Name)) {
						continue;
					}

					String[] twoTerms = twoTerms(doc1.getTerms(), doc2.getTerms());
					StringBuilder vector1 = new StringBuilder(256);
					StringBuilder vector2 = new StringBuilder(256);

					for (int i = 0, size = twoTerms.length; i < size; i++) {
						vector1.append(doc1.contain(twoTerms[i])).append(",");
						vector2.append(doc2.contain(twoTerms[i])).append(",");
					}

					String docName = doc1Name + "@" + doc2Name;
					String docvector1 = vector1.toString().substring(0, vector1.length() - 1);
					String docvector2 = vector2.toString().substring(0, vector2.length() - 1);

					twoDocs.append(docName).append("\t");
					twoDocs.append(docvector1).append("\n");

					twoDocs.append(docName).append("\t");
					twoDocs.append(docvector2).append("\n");

					String docKeyMap = twoDocs.toString();
					logger.info("(" + parsingLine + ")" + docKeyMap);
					FileUtils.writeStringToFile(new File(out), docKeyMap, "utf-8", true);
					twoDocs.setLength(0);
					parsingLine++;
				}
			}

		} catch (FileNotFoundException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} finally {
			br.close();
		}
	}

	private void deleteFile(String out) {
		File file = new File(out);
		if (file.exists()) {
			file.delete();
		}

	}

	private String[] twoTerms(String[] terms1, String[] terms2) {
		Set<String> termSet = new HashSet<String>();
		termSet.addAll(Arrays.asList(terms1));
		termSet.addAll(Arrays.asList(terms2));

		String[] twoTerms = termSet.toArray(new String[termSet.size()]);
		return twoTerms;
	}
}
