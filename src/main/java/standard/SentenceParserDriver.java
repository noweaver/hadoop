package standard;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

public class SentenceParserDriver extends UDF{

	public Text evaluate() {
		return null;
		
	}
	
	private String extract_korean_sentence(String column, int sentenceNum) {
		String filter[] = { "니다", "없다", "있다", "니까", "이다", "까요" };

		int sentenceCnt = 0;
		int beforeCnt = 0;
		StringBuilder temp = new StringBuilder(100);
		StringBuilder sentences = new StringBuilder(256);
		for (int i = 0, size_i = column.length(); i < size_i; i++) {
			if (sentenceCnt > 4) {
				break;
			}
			char firtCha = column.charAt(i);
			char secondCha = column.charAt(i + 1);

			temp.append(firtCha).append(secondCha);

			for (int j = 0, size_j = filter.length; j < size_j; j++) {
				if (filter[j].equals(temp.toString())) {
					sentenceCnt++;

					break;
				}
			}

			temp.setLength(0);
			sentences.append(firtCha);
			if (beforeCnt != sentenceCnt) {
				sentences.append(secondCha);
				beforeCnt = sentenceCnt;
				i++;
			}
		}
		return sentences.toString();
	}
}
