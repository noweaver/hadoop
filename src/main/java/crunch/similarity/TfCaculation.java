package crunch.similarity;

import org.apache.crunch.DoFn;
import org.apache.crunch.Emitter;
import org.snu.ids.ha.index.Keyword;
import org.snu.ids.ha.index.KeywordExtractor;
import org.snu.ids.ha.index.KeywordList;

public class TfCaculation extends DoFn<String, String> {
	private static final long serialVersionUID = -1240466145326986047L;

	@Override
	public void process(String line, Emitter<String> emitter) {
		String[] splitedLine = line.split("\t");
		String content = splitedLine[1];

		KeywordList kl = new KeywordExtractor().extractKeyword(content, true);
		StringBuilder sb = new StringBuilder(512);

		for (int i = 0; i < kl.size(); i++) {
			Keyword keyword = kl.get(i);

			String term = keyword.getString();
			if (term.length() < 2) {
				continue;
			}

			int cnt = keyword.getCnt();
			double tf = keyword.getFreq();

			sb.append(splitedLine[0]).append("\t").append(term).append("\t").append(cnt).append("\t").append(tf);
			emitter.emit(sb.toString());
			sb.setLength(0);
		}
	}

}
