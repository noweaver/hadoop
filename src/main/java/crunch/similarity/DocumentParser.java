package crunch.similarity;

import java.util.TreeMap;

import org.apache.crunch.DoFn;
import org.apache.crunch.Emitter;

public class DocumentParser extends DoFn<String, TreeMap<String, String>> {
	private static final long serialVersionUID = -1971127406458745105L;

	@Override
	public void process(String line, Emitter<TreeMap<String, String>> emitter) {
		TreeMap<String, String> parsedLine = new TreeMap<String, String>();
		StringBuilder termNvector = new StringBuilder(1024);

		if (line.length() > 50) {
			String[] splitedLine = line.split("[,]");

			double vector = Double.parseDouble(splitedLine[4]);
			if (vector > 0) {
				termNvector.append(splitedLine[3]).append(",");
			} else {
				termNvector.append("").append(",");
			}
			termNvector.append(vector);

			parsedLine.put(splitedLine[0], termNvector.toString());
		}

		emitter.emit(parsedLine);
	}

	// @Override
	// public void process(String input, Emitter<TreeMap<String, String>> emitter) {
	// // TODO Auto-generated method stub
	//
	// }

}
