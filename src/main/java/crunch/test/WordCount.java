package crunch.test;

import org.apache.crunch.DoFn;
import org.apache.crunch.Emitter;
import org.apache.crunch.PCollection;
import org.apache.crunch.PTable;
import org.apache.crunch.Pipeline;
import org.apache.crunch.PipelineResult;
import org.apache.crunch.impl.mr.MRPipeline;
import org.apache.crunch.io.To;
import org.apache.crunch.types.writable.Writables;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class WordCount extends Configured implements Tool {

	public static void main(String[] args) throws Exception {
		ToolRunner.run(new Configuration(), new WordCount(), args);
	}

	public int run(String[] args) throws Exception {
		if (args.length != 2) {
			System.err.println(
					"Usage: hadoop jar snippet-hadop-0.0.1-SNAPSHOT-job.jar" + " [generic options] input output");
			System.err.println();
			GenericOptionsParser.printGenericCommandUsage(System.err);
			return 1;
		}

		// Create an object to coordinate pipeline creation and execution.
		Pipeline pipeline = new MRPipeline(WordCount.class, getConf());

		// Reference a given text file as a collection of Strings.
		PCollection<String> lines = pipeline.readTextFile(args[0]);

		// Define a function that splits each line in a PCollection of Strings into
		// PCollection made up of the individual words in the file.
		// PCollection<String> words = lines.parallelDo(new DoFn<String, String>() {
		// private static final long serialVersionUID = 1L;
		//
		// public void process(String line, Emitter<String> emitter) {
		// for (String word : line.split("\\s+")) {
		// emitter.emit(word);
		// }
		// }
		// }, Writables.strings()); // Indicates the serialization format

		PCollection<String> words = lines.parallelDo(new Tokenized(), Writables.strings());

		// The count method applies a series of Crunch primitives and returns
		// a map of the unique words in the input PCollection to their counts.
		// Best of all, the count() function doesn't need to know anything about
		// the kind of data stored in the input PCollection.
		PTable<String, Long> counts = words.count();

		// words.count().write(To.textFile(args[1]));
		// Instruct the pipeline to write the resulting counts to a text file.
		pipeline.writeTextFile(counts, args[1]);

		// Execute the pipeline as a MapReduce.
		PipelineResult result = pipeline.done();

		// pipeline.done();
		return result.succeeded() ? 0 : 1;
	}
}
