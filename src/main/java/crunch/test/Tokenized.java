package crunch.test;

import org.apache.crunch.DoFn;
import org.apache.crunch.Emitter;

public class Tokenized extends DoFn<String, String> {
	private static final long serialVersionUID = -4499719470669507349L;

	@Override
	public void process(String line, Emitter<String> emitter) {
		for (String word : line.split("\\s+")) {
			emitter.emit(word);
		}
	}

}
