package format.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import format.org.TestReadWriteParquet;
import parquet.Log;
import parquet.example.data.Group;
import parquet.hadoop.ParquetFileReader;
import parquet.hadoop.ParquetInputSplit;
import parquet.hadoop.example.ExampleInputFormat;
import parquet.hadoop.example.GroupWriteSupport;
import parquet.hadoop.metadata.ParquetMetadata;
import parquet.schema.MessageType;

public class ReadParquetMR extends Configured implements Tool {
	private static final Log LOG = Log.getLog(TestReadWriteParquet.class);

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new ReadParquetMR(), args);
		System.exit(res);
	}

	private static class FieldDescription {
		public String constraint;
		public String type;
		public String name;
	}

	private static class RecordSchema {
		public RecordSchema(String message) {
			fields = new ArrayList<FieldDescription>();
			List<String> elements = Arrays.asList(message.split("\n"));
			Iterator<String> it = elements.iterator();
			while (it.hasNext()) {
				String line = it.next().trim().replace(";", "");
				;
				System.err.println("RecordSchema read line: " + line);
				if (line.startsWith("optional") || line.startsWith("required")) {
					String[] parts = line.split(" ");
					FieldDescription field = new FieldDescription();
					field.constraint = parts[0];
					field.type = parts[1];
					field.name = parts[2];
					fields.add(field);
				}
			}
		}

		private List<FieldDescription> fields;

		public List<FieldDescription> getFields() {
			return fields;
		}
	}

	/*
	 * Read a Parquet record, write a CSV record
	 */
	public static class ReadRequestMap extends Mapper<LongWritable, Group, NullWritable, Text> {
		private static List<FieldDescription> expectedFields = null;

		@SuppressWarnings("deprecation")
		@Override
		public void map(LongWritable key, Group value, Context context) throws IOException, InterruptedException {
			NullWritable outKey = NullWritable.get();
			
			InputSplit inputSplit = context.getInputSplit();
			
			Path path = ((ParquetInputSplit)context.getInputSplit()).getPath();
			
			
			if (expectedFields == null) {
				// Get the file schema which may be different from the fields in
				// a particular record) from the input
				// split
				
				((ParquetInputSplit)context.getInputSplit()).getFileSchema();
				
				String fileSchema = ((ParquetInputSplit) context.getInputSplit()).getFileSchema();
				// System.err.println("file schema from context: " +
				// fileSchema);
				RecordSchema schema = new RecordSchema(fileSchema);
				expectedFields = schema.getFields();
				// System.err.println("inferred schema: " +
				// expectedFields.toString());
			}

			// No public accessor to the column values in a Group, so extract
			// them from the string representation
			String line = value.toString();
			String[] fields = line.split("\n");

			StringBuilder csv = new StringBuilder();
			boolean hasContent = false;
			int i = 0;
			// Look for each expected column
			Iterator<FieldDescription> it = expectedFields.iterator();
			while (it.hasNext()) {
				if (hasContent) {
					csv.append(',');
				}
				String name = it.next().name;
				if (fields.length > i) {
					String[] parts = fields[i].split(": ");
					// We assume proper order, but there may be fields missing
					if (parts[0].equals(name)) {
						boolean mustQuote = (parts[1].contains(",") || parts[1].contains("'"));
						if (mustQuote) {
							csv.append('"');
						}
						csv.append(parts[1]);
						if (mustQuote) {
							csv.append('"');
						}
						hasContent = true;
						i++;
					}
				}
			}
			context.write(outKey, new Text(csv.toString()));
		}
	}

	public int run(String[] args) throws Exception {
		Configuration conf = getConf();
		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(args[1]), true);

		conf.set("mapreduce.output.textoutputformat.separator", ",");

		Path parquetFilePath = null;
		// Find a file in case a directory was passed
		RemoteIterator<LocatedFileStatus> it = FileSystem.get(conf).listFiles(new Path(args[0]), true);
		while (it.hasNext()) {
			FileStatus status = it.next();
			if (status.isFile()) {
				parquetFilePath = status.getPath();
				break;
			}
		}
		if (parquetFilePath == null) {
			LOG.error("No file found for " + args[0]);
			return 1;
		}
		LOG.info("Getting schema from " + parquetFilePath);
		ParquetMetadata readFooter = ParquetFileReader.readFooter(conf, parquetFilePath);
		MessageType schema = readFooter.getFileMetaData().getSchema();
		LOG.info(schema);
		GroupWriteSupport.setSchema(schema, conf);

		Job job = Job.getInstance(conf);
		job.setJarByClass(getClass());
		job.setJobName(getClass().getName());

		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(ReadRequestMap.class);
		job.setNumReduceTasks(0);

		job.setInputFormatClass(ExampleInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);

		return 0;
	}

}
