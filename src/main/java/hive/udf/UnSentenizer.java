package hive.udf;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.Text;

public class UnSentenizer extends UDF {
	public Text evaluate(ArrayWritable columns) {
		String[] strs = columns.toStrings();
		StringBuilder sb = new StringBuilder(256);
		
		for (String str : strs) {
			sb.append(str).append(". ");
		}

		// String data = sb.substring(0, sb.toString().length()-1);
		return new Text(sb.toString().trim());
	}
}
