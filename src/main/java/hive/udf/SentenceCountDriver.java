package hive.udf;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class SentenceCountDriver extends UDF {
	public IntWritable evaluate(Text column) {
		String filter[] = { "니다", "없다", "있다", "니까", "이다", "까요" };
		
		String sentences = column.toString();
		
		int cnt = 0;
		for (int i = 0, size = filter.length; i < size; i++) {

			int pos = 0, end = 0;
			String substring = null;
			while ((end = sentences.indexOf(filter[i], pos)) >= 0) {
				substring = sentences.substring(end, end + 2);
				if (filter[i].equals(substring)) {
					cnt++;
				}
				pos = end + 1;
			}
		}

		return new IntWritable(cnt);
	}
}
