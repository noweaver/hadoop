package hive.udf;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SentenceCountDriverTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		String sen1 = "제자리에 머무는 사람에게는 발전이 없다";
		String sen2 = " 이는 좌우명입니다";
		String sen3 = " 어릴때부터 키워왔습니다";
		String sen4 = " 그 꿈을 당연한 결과라고 생각합니다";
		String sen5 = " 대학교에서 맘껏 발휘하고 싶습니다";
		String sen6 = " 그리고 가장 적합하다고 생각하기 때문입니다";
		String sen7 = " 그리고 개발 부문에 지원하게 되었습니다";
		String sen8 = " 작은 시켜보겠습니다";
		String sen9 = " 그리하여 현재 세계시장을 주름잡고 있는 일조하겠습니다";

		StringBuilder allSentence = new StringBuilder();
		allSentence.append(sen1).append(sen2).append(sen3).append(sen4).append(sen5);
		//allSentence.append(sen6).append(sen7).append(sen8).append(sen9);
		
		SentenceCountDriver driver = new SentenceCountDriver();
		Assert.assertEquals(new IntWritable(5), driver.evaluate(new Text(allSentence.toString())));
	}

}
