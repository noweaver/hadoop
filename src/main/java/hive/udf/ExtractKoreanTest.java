package hive.udf;

import org.apache.hadoop.io.Text;
import org.junit.Before;
import org.junit.Test;

public class ExtractKoreanTest {

	private ExtractKorean ek;
	
	@Before
	public void setUp() throws Exception {
		ek = new ExtractKorean();
	}

	@Test
	public void test() {
		String hanJa = " 向世界第一名的挑战 一定要认识中国         您好  我是金融专业四年级学生吴藇喜  首先  我來介紹我在大學怎麼過生活  毕业将至  又一个新开始即将来到  等待着我继续努力奋斗 迎接挑战 时光飞梭 将带着童年的梦想 青年的理想离开学校  走上工作岗位  大学四年是我思想 知识结构及心理 生长成熟的四年 管理大学的浓厚学习 创新氛围 熔融其中四年使我成为一名复合型人才     在大学四年是我思想 知识结构及心理 生长成熟的四年 在大学期间  我认真学习专业技能  掌握了较强的专业知识  并把理论知识运用到实践中去  获得台湾政府的语言奖学金和中国国家汉办的汉语水平考试奖学金  此外  我还加入了    首尔核安保会议网路记者和学校的记者  在院学生会期间  表现突出  由干事被选拔为院社团联合会管理部副部长  后来  转而做学生通讯 社负责人  作为新校区学生通讯 社的负责人  负责新校区学生通讯社许多的工作  在院党委宣传部的指导下  经过学生们的团结努力  从无到有  成功地组建了新校区学生通讯社 并在接下来的一年中  又成功地组织了三校区学生通讯 社记者培训工作  三校区学生通讯社辩论赛  在此期间  由于工作努力  表现突出  被院党委宣传部评为 优秀学生干部      其次  我想寫下來  为什么我要當現代汽車的實習生 现代汽车以全新的外貌挑战世界的舞台  而我愿意成为现代汽车更好发展的引导者      年 我第一去台湾的时候  周围的人认为   现代  这个品牌是亚洲一家制造低廉品牌车的公司  但是随着是时间的流逝  进攻性的市场战略  革新的设计  卓越的性能  现代汽车充满了台湾的大街小巷  特别是当我作为顾客搭坐计程车时候  听到他们讲说现代汽车最具安全性的时候 我感到非常的自豪  以此同时  现代这个品牌不仅仅是汽车 还通过橄榄球 高尔夫 网球等各种体育赞助和慈善公司 已经渐渐渗入到外国人的日常生活中  我们可以看到现代品牌的惊人发展      年 现代汽车致使日本汽车企业的攻势在猛烈的北美市场中活跃性减弱  在美国 过去的两个月中 现代小型  中型的汽车的出售量包揽了第一位 在加拿大 现代汽车创下了每年七月中的最高业绩  现代汽车正在用自身在给全世界的人传达我们国家的技术与品质  因此  我认为在现代汽车公司中工作和提高韩国的价值是一样的  我想亲身在公司中去引导现代公司成长  与现代汽车一起变化 来实现企业的革新  毫无疑问现代公司会拥有一双翅膀飞向世界  我会坚持不解的挑战世界的变化与革新  满怀自信的在现代公司中工作的    如果 您给我机会的话 我会尽量发挥我的能力 给您很满意的感觉 谢谢您 ";
		String garbage = "그는 조엘이다. 나는 라이언이다! 그는 누구일까?ㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎㅎ";
		String french = "J aime s entendre avec les étrangers depuis longtemps   Alors  je suis entrée dans le département de langue et littérature françaises parce ";
		String russia = "В сентября        я уехала в Санкт петербург по обмену  В нашем университете говорится это легенда  потому что это вызов   ";
		String japanese = "情熱は 私の能力を作り出し 私の努力を作り出し 私の夢を作り出してくれるものだと信じています 私の情熱は自動車のヘッドライトに表現してみたいです 最も前に位置し 暗闇の中でも雨が降るなどの環境の中でも私を目的地まで導いてくれる明るい光のように 私の情熱は周りのどんな悪条件にも 更に明るい光を発して私が目標とする場所にまで 私を鞭打ちながら導いてくれる私の力の源泉であり 私が全てのことに臨む基本です     見解を広げようと思う情熱と貿易業を営む父の積極的な支持で 幼い頃から中国と日本で暮らして教育を受けたことがあります 高校生の時期を中国で送り 孔子の思想を土台とする道徳的義務を学びました 日本では一人でやっていくのではなく 互いに協力して共通の目標を達成するやり方を学びました 特に 最も成長期の  代の時期を外国で送りながら 試行錯誤もありましたが現地の友人達との交流を通じて より広い視野と会話の実力を身に付けることができました それだけでなく 多彩な経験によって自然に どんなところのどんな層の集団とでも合わせていける適応力を身に付けました     海外生活をする中で 韓国企業の活躍を見て 私が韓国人であることが誇らしく 大きな自負心を感じました その中心には現代自動車がいて 国内のみならず自動車王国と呼ばれているアメリカでも最も高い再購入率を記録     年のブランド再購入率の調査で再購入率   と 現代自動車が 位を占め 世界市場でも認められていることを胸が高鳴るほど誇らしく思いました それで その一部になって目覚しい活躍に同参したいと思い 現代自動車で私の夢を広げてみせようという目標を胸に刻みました まだ新社会人で 足りない点が多いのですが 今まで学んできた理論や多様な経験を土台に 世界に現代自動車を広く知らしめるのに貢献し 更に数年後にはその中心で最高の専門家になり チーム員の一員として現代自動車を率いていきたいと 現代自動車に志願することにしました     私の自動車に対する情熱は 小学校 年生の時 父が初めて購入した自動車から始まりました その時から 他の自動車の名前などを次々と綴り デザインの特性などを落書き帳に描いてみるのが私の趣味となりました 学業が忙しくなり いつの間にか自動車に対する関心も忘れたまま 中国 日本 アメリカに留学し 日本文化に関心があって UC Berkeleyに進学して日本語専攻を選択しました けれども     年に自動車王国と呼ばれるアメリカに行って様々な自動車に接し その中でも韓国の自動車の技術の評判と現実を見て 私の自動車に対する情熱はまた目覚めたのでした それで私が学んだ ヶ国語と文化の多様性を理解できる能力を自動車に利用できる分野 そしてグローバルな世界とコミュニケーションできる現代自動車のインターングローバルコミュニケーターに志願させていただくことにいたしました 既に優れた安定性と無駄な故障がないというイメージで 他の自動車メーカーより大きい市場と世界的な位置を占めているため 私が私の能力と夢を広げて情熱を表現できる理想的な会社です    上に羅列したように 私は現代自動車のグローバルコミュニケーターとして仕事をするために準備された人材であると自負しております ";
		String mix = "My name is Roy and I am a recent graduate of ArtCenter College of Design         I was born in Korea  but also lived in LA  Denver  and NY throughout my life  Interior designer mother and architect father were not happy when I would say  I wanted to grow up to become a taxi driver  I decided to pursue my attachment to cars and applied to ArtCenter  During my time at ArtCenter  I was blessed to find my significant other and got married during the terms        Before coming back to school to finish my degree at ArtCenter  I was fortunate enough to work professionally in various fields  This allowed me to widen my horizon as a designer  The more I gained knowledge and experience  I realized there is always more to learn  And as a new graduate  my immediate goal would be  to learn as much as possible and assist the team with all my ability  So that in the near future I can become an important member of the team         I believe the unique perspective that I have developed throughout the years will be a great addition to the team  But the more important aspect about myself is that  I am a self starter and I am a person that is determined to get results  If I am allowed to dedicate my time at the studio  I will be committed to making our team succeed     Sincerely     노영균  Roy Roh  sketchgorilla gmail com               고화질 포트폴리오와 레쥬매는 아래 링크에서 다운받으실 수 있습니다   https   www box com sh x ggx bih gsku  AABqaKIOnIR S qWA j    Ia";
		String test = "시간이 지나 그분들의 생각 아르바이트생으로는 파악하기 어려운 공장설비의 진행상황 등을 파악할 수 있었고 유익한 시간을 보낼 수 있었습니다.저의 단점은 낙천적인 성격과 풍부한 감수성입니다.낙천적인 성격 때문에 객관적으로 바라봐야 할 상황에 판단력이 흐려지기도 하고 감수성이 풍부해서 감정에 휩싸이기도 합니다. ";
		
//		Text evaluate = ek.evaluate(new Text(hanJa));
//		System.out.println(evaluate.toString().length() + ":" + evaluate.toString());
		
		test = ".&lt;현대자동차와 인재의 아름다운 내일을 돕는 사람이 되고 싶습니다.&gt;"; 
		test = ".다음 중 ‘이오송’이 현대자동차 국내영업에 채용되어야 하는 이유는?] ①{ 적성 } [ 영업에 미치다! ] ";
		test = ".&lt; ...지원동기 ??&gt;[ 경쟁력!!! 핵심 ‘품질’ + 구매 = 산업공학 ] 현대자동차 국내영업부서는 고객과 시장의 접점에서 회사 제품에 대한 영업관리 ";
		Text evaluate = ek.evaluate(new Text(test));
		System.out.println(evaluate.toString().length() + ":" + evaluate.toString());
		
//		evaluate = ek.evaluate(new Text(french));
//		System.out.println(evaluate.toString().length() + ":" + evaluate.toString());
//		
//		evaluate = ek.evaluate(new Text(russia));
//		System.out.println(evaluate.toString().length() + ":" + evaluate.toString());
//		
//		evaluate = ek.evaluate(new Text(japanese));
//		System.out.println(evaluate.toString().length() + ":" + evaluate.toString());
//		
//		evaluate = ek.evaluate(new Text(mix));
//		System.out.println(evaluate.toString().length() + ":" + evaluate.toString());
	}

}
