package hive.udf;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SentenceParserDriverTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testUDF() {
		String sen1 = "제자리에 머무는 사람에게는 발전이 없다" + " 이는 좌우명입니다" + " 어릴때부터 키워왔습니다" + " 그 꿈을 당연한 결과라고 생각합니다"
				+ " 대학교에서 맘껏 발휘하고 싶습니다" + " 그리고 가장 적합하다고 생각하기 때문입니다" + " 그리고 개발 부문에 지원하게 되었습니다" + " 작은 시켜보겠습니다"
				+ " 그리하여 현재 세계시장을 주름잡고 있는 일조하겠습니다";

		Text expect1 = new Text("제자리에 머무는 사람에게는 발전이 없다");
		Text expect2 = new Text("제자리에 머무는 사람에게는 발전이 없다" + " 이는 좌우명입니다");
		Text expect3 = new Text("제자리에 머무는 사람에게는 발전이 없다" + " 이는 좌우명입니다" + " 어릴때부터 키워왔습니다");
		Text expect4 = new Text("제자리에 머무는 사람에게는 발전이 없다" + " 이는 좌우명입니다" + " 어릴때부터 키워왔습니다" + " 그 꿈을 당연한 결과라고 생각합니다");
		Text expect5 = new Text("제자리에 머무는 사람에게는 발전이 없다" + " 이는 좌우명입니다" + " 어릴때부터 키워왔습니다" + " 그 꿈을 당연한 결과라고 생각합니다"
				+ " 대학교에서 맘껏 발휘하고 싶습니다");

		SentenceParserDriver driver = new SentenceParserDriver();

//		Assert.assertEquals(expect1, driver.evaluate(new Text(sen1), new IntWritable(1)));
//		Assert.assertEquals(expect2, driver.evaluate(new Text(sen1), new IntWritable(2)));
//		Assert.assertEquals(expect3, driver.evaluate(new Text(sen1), new IntWritable(3)));
//		Assert.assertEquals(expect4, driver.evaluate(new Text(sen1), new IntWritable(4)));
		Assert.assertEquals(expect5, driver.evaluate(new Text(sen1), new IntWritable(5)));
	}

	@Test
	public void testOutOfIndexCheck() {
		String sen1 = "성장하는 연구원 년 간 광소자의 특성평가 분석을 하며 관련 전문지식을 습득했습니다 이를 바탕으로 현재 불량분석을 주로 담당하며 실제 제품이 사용되는 과 등에 관심을 갖게 되었고 신소재 전공자로서 전자재료 뿐 아닌 금속을 아우르는 소재분석 전문가로 발전하고 싶어 지원합니다 이를 위해 누구보다 체험하고 경험하는 실천적인 연구원이 되고자 합니다 제품분석 을 통한 경쟁사 제품의 특성평가 및 자사제품과 비교분석 신기술 동향 파악 개발방향 제시 불량분석 불량 현상 발생 원인 분석 및 특성 향상 위한 소재분석 담당 분석 장비 이외 자료해석 가능 장비 적분구 열화상카메라";
		Text expect1 = new Text("성장하는 연구원 년 간 광소자의 특성평가 분석을 하며 관련 전문지식을 습득했습니다");
		SentenceParserDriver driver = new SentenceParserDriver();
		
		Assert.assertEquals(expect1, driver.evaluate(new Text(sen1), new IntWritable(1)));
	}

}
