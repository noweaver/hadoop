package hive.udf;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class SentenceParserDriver extends UDF{
	
	public Text evaluate(Text column, IntWritable sentenceNum) {
		
		if (sentenceNum == null || sentenceNum.get() == 0) {
			//
		}
		
		String filter[] = { "니다", "없다", "있다", "니까", "이다", "까요" };
		
		int sentenceCnt = 0;
		int beforeCnt = 0;
		StringBuilder temp = new StringBuilder(100);
		StringBuilder sentences = new StringBuilder(256);
		
		String data = column.toString();
		int num = sentenceNum.get();
		
		for (int i = 0, size_i = data.length(); i < size_i; i++) {
			if (sentenceCnt >= num) {
				break;
			}
			char firtCha = data.charAt(i);
			System.out.println(firtCha);
			char secondCha = data.charAt(i + 1);
			System.out.println(secondCha);

			temp.append(firtCha).append(secondCha);

			for (int j = 0, size_j = filter.length; j < size_j; j++) {
				if (filter[j].equals(temp.toString())) {
					sentenceCnt++;

					break;
				}
			}

			temp.setLength(0);
			sentences.append(firtCha);
			if (beforeCnt != sentenceCnt) {
				sentences.append(secondCha);
				beforeCnt = sentenceCnt;
				i++;
			}
		}
		
		return new Text(sentences.toString());		
	}
	//
	// private String extract_korean_sentence(String column, int sentenceNum) {
	// String filter[] = { "니다", "없다", "있다", "니까", "이다", "까요" };
	//
	// int sentenceCnt = 0;
	// int beforeCnt = 0;
	// StringBuilder temp = new StringBuilder(100);
	// StringBuilder sentences = new StringBuilder(256);
	// for (int i = 0, size_i = column.length(); i < size_i; i++) {
	// if (sentenceCnt > 4) {
	// break;
	// }
	// char firtCha = column.charAt(i);
	// char secondCha = column.charAt(i + 1);
	//
	// temp.append(firtCha).append(secondCha);
	//
	// for (int j = 0, size_j = filter.length; j < size_j; j++) {
	// if (filter[j].equals(temp.toString())) {
	// sentenceCnt++;
	//
	// break;
	// }
	// }
	//
	// temp.setLength(0);
	// sentences.append(firtCha);
	// if (beforeCnt != sentenceCnt) {
	// sentences.append(secondCha);
	// beforeCnt = sentenceCnt;
	// i++;
	// }
	// }
	// return sentences.toString();
	// }
}
