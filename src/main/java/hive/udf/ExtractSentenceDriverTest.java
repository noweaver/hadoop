package hive.udf;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.avro.io.BufferedBinaryEncoder;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.CharMatcher;

public class ExtractSentenceDriverTest {
	private ExtractSentenceDriver driver;

	@Before
	public void setUp() throws Exception {
		driver = new ExtractSentenceDriver();
	}

	@Test
	public void testUDF() {
		String sen1 = "제자리에 머무는 사람에게는 발전이 없다";
		String sen2 = " 이는 좌우명입니다";
		String sen3 = " 어릴때부터 키워왔습니다";
		String sen4 = " 그 꿈을 당연한 결과라고 생각합니다";
		String sen5 = " 대학교에서 맘껏 발휘하고 싶습니다";
		String sen6 = " 그리고 가장 적합하다고 생각하기 때문입니다";
		String sen7 = " 그리고 개발 부문에 지원하게 되었습니다";
		String sen8 = " 작은 시켜보겠습니다";
		String sen9 = " 그리하여 현재 세계시장을 주름잡data고 있는 일조하겠습니다";

		StringBuilder allSentence = new StringBuilder();
		allSentence.append(sen1).append(sen2).append(sen3).append(sen4).append(sen5);
		allSentence.append(sen6).append(sen7).append(sen8).append(sen9);

		// String sen1 = "제자리에 머무는 사람에게는 발전이 없다" + " 이는 좌우명입니다" + " 어릴때부터 키워왔습니다" + " 그 꿈을 당연한 결과라고 생각합니다"
		// + " 대학교에서 맘껏 발휘하고 싶습니다" + " 그리고 가장 적합하다고 생각하기 때문입니다" + " 그리고 개발 부문에 지원하게 되었습니다" + " 작은 시켜보겠습니다"
		// + " 그리하여 현재 세계시장을 주름잡고 있는 일조하겠습니다";
		// String sen1 = "제자리에 머무는 사람에게는 발전이 없다";

		Text expect1 = new Text(sen1);
		Text expect2 = new Text(sen1 + sen2);
		Text expect3 = new Text(sen1 + sen2 + sen3);
		Text expect4 = new Text(sen1 + sen2 + sen3 + sen4);
		Text expect5 = new Text(sen1 + sen2 + sen3 + sen4 + sen5);

		ExtractSentenceDriver driver = new ExtractSentenceDriver();

		// Assert.assertEquals(expect1, driver.evaluate(new Text(sen1), new IntWritable(1)));
		// Assert.assertNotEquals(expect2, driver.evaluate(new Text(sen1), new IntWritable(2)));
		// Assert.assertEquals(expect2, driver.evaluate(new Text(sen1 + sen2), new IntWritable(2)));
		// Assert.assertEquals(expect3, driver.evaluate(new Text(expect3), new IntWritable(3)));
		// Assert.assertEquals(expect4, driver.evaluate(new Text(expect4), new IntWritable(4)));
		// Assert.assertEquals(expect5, driver.evaluate(new Text(expect5), new IntWritable(5)));

		// Assert.assertEquals(expect1, driver.evaluate(new Text(allSentence.toString()), new IntWritable(1)));
		// Assert.assertEquals(expect2, driver.evaluate(new Text(allSentence.toString()), new IntWritable(2)));
		Assert.assertEquals(expect5, driver.evaluate(new Text(allSentence.toString()), new IntWritable(5)));
	}

	@Test
	public void testOutOfIndexCheck() {
		ExtractSentenceDriver driver = new ExtractSentenceDriver();

		// String sen1 = "성장하는 연구원 년 간 광소자의 특성평가 분석을 하며 관련 전문지식을 습득했습니다 이를 바탕으로 현재 불량분석을 주로 담당하며 실제 제품이 사용되는 과 등에 관심을 갖게
		// 되었고 신소재 전공자로서 전자재료 뿐 아닌 금속을 아우르는 소재분석 전문가로 발전하고 싶어 지원합니다 이를 위해 누구보다 체험하고 경험하는 실천적인 연구원이 되고자 합니다 제품분석 을 통한 경쟁사
		// 제품의 특성평가 및 자사제품과 비교분석 신기술 동향 파악 개발방향 제시 불량분석 불량 현상 발생 원인 분석 및 특성 향상 위한 소재분석 담당 분석 장비 이외 자료해석 가능 장비 적분구
		// 열화상카메라";
		// Text expect1 = new Text("성장하는 연구원 년 간 광소자의 특성평가 분석을 하며 관련 전문지식을 습득했습니다");
		//
		// Assert.assertEquals(expect1, driver.evaluate(new Text(sen1), new IntWritable(1)));

		String sentence = "0123456789";
		Assert.assertEquals("", driver.evaluate(new Text(sentence), new IntWritable(1)));
	}

	@Test
	public void testSetence() throws Exception {
		String sen = "다시 태어나도 내 회사 인사 담당자님께 질문 드립니다 다시 태어나셔도 현대자동차에 입사하시겠습니까 저는 다시 태어나도 현대자동차에 입사할 겁니다 제가 너무나 사랑하는 회사니까요 현대 자동차 채용 설명회 당시 제 가슴에 박힌 말입니다 내부 구성원이 이렇듯 온 마음을 다해 헌신하는 회사를 누가 들어가고 싶어하지 않겠습니까 인사 담당자 님의 말씀에 현대자동차의 강점을 또 한 가지 알게 되었습니다 이제 저 또한 당당히 외치고 싶습니다 제 모든 것을 바칠 제 회사 현대 자동차를 사랑한다고 말입니다 세대공감 우리를 제일 잘 이해해주는 선생님 약 년 간 학원 강사로 근무하면서 학부모님과 아이들에게 가장 많이 들었던 말입니다 대부터 이 넘으신 학부모님들까지 다양한 성격의 사람들을 만나 이야기를 듣고 공감하며 년을 보냈습니다 소통이 시대의 화두인 지금 대부터 대까지 다양한 연령대의 사람들을 꾸준히 만나 그들의 삶의 이야기를 들을 수 있는 기회는 제 또래에서는 많이 없다고 생각합니다 현대자동차의 고객도 마찬가지입니다 갓 면허를 딴 대에서 대까지 영업 지원은 년 년 혹은 년 이상이 될 지도 모르는 그들의 자동차와 동행하며 소통하는 자리라고 생각합니다 제가 그 역할의 선봉장이 되겠습니다 의 무대 넌 못하는 게 뭐야 유리야 넌 모르는 게 없는 것 같아 이 세상에는 한 분야에 정통한 수많은 가 있습니다 저는 는 아닙니다 하지만폭 넓은 분야에 대한 지식을 바탕으로 수많은 고객들의 니즈를 파악하고 이를 이용하여 높은 성과를 이루어낼 수 있는 능력 저 최유리가 가지고 있습니다 고객관리부터 후속조치까지 고객의 바로 뒤에서 직접 발로 뛰어야 하는 영업지원이야말로 수많은 들의 무대라고 생각합니다 이 무대에서 제가 가진 역량을 최대한 발휘하겠습니다";
		String sen2 = "생명의 은인그리고 내 인생의 은인 현대 자동차는 제 아버지의 생명의 은인이며 제가 가장 존경하는 분께서 창립한 회사입니다 제가 초등학교 학년 때 여행을 다녀오시던 중 아버지께서는 큰 사고가 나셨습니다 당시 사망자가 많아 뉴스에도 나올 정도였는데 아버지께서는 당시 타고 계시던 스타렉스 좌석 부분이 형태가 유지되어 간신히 목숨을 구하셨다고 합니다 그 뒤로 우리집 차는 쭉 현대 자동차였고 입사의 기회가 주어진다면 이런 안전한 차를 더 많은 사람들에게 팔고 싶다고 생각했습니다 故 아산 정주영 선생님은 제 인생의 모토가 되어주시는 특별한 분입니다 십여 년 전 우연히 아산 정주영 선생님의 행적에 대해 조사하게 되었고 그 뒤로 제가 가장 존경하는 분이 되었습니다 제 인생에 위기가 올 때마다 아산 선생님의 일화를 보며 힘을 냈고 지금까지 올 수 있게 되었습니다 이번 공채에서 근자감 있는 인재를 찾는단 소식에 될 수 있다는 의 확신과 의 자신감을 가지고 해외 영업 부문에 지원하게 되었습니다 해외 영업은 제가 고등학교 때부터 꿈꾸던 직무였습니다 사람 만나기 좋아하고 남들보다 뛰어난 의사 소통 능력을 가지고 있는 제게 적격이라고 생각했기 때문입니다 외국인 친구 사귀는 것을 좋아하고 그들과 문화 장벽 없이 잘 지낼 수 있는 것 또한 해외 영업에 잘 맞는다고 생각합니다 아주 유창한 영어 실력은 아니지만 무리없이 영어로 의사 소통이 가능하고 지금도 끊임없이 노력 중이기 때문에 문제가 되지 않는다고 생각합니다 또한 년간 영어 강사로 근무하면서 쌓아온 스킬로 각종 비즈니스 서류 작성에 발군의 실력을 발휘하겠습니다";

		Text evaluate = driver.evaluate(new Text(sen), new IntWritable(5));
		System.out.println(evaluate.toString());

		evaluate = driver.evaluate(new Text(sen2), new IntWritable(5));
		System.out.println(evaluate.toString());
	}

	@Test
	public void testMalformed() throws IOException {
		String filePath = "D:/Workspace/Git/snippet-hadoop/data/temp/000000_0"; // 145MB
		BufferedReader br = null;

		try {
			int lineNumber = 1;
			br = new BufferedReader(new FileReader(new File(filePath)));
			String line = null;
			while ((line = br.readLine()) != null) {
				line = CharMatcher.JAVA_ISO_CONTROL.removeFrom(line);
				System.out.println("["+ lineNumber + "] "+ line);
				
				
				Text evaluate = driver.evaluate(new Text(line), new IntWritable(5));
				System.out.println("["+ lineNumber++ + "] " + evaluate);
				System.out.println("##################################");
			}
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		} finally {
			br.close();
		}
	}
}
