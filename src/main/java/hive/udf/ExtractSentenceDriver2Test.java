package hive.udf;

import static org.junit.Assert.*;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExtractSentenceDriver2Test {

	private ExtractSentenceDriver2 driver;

	@Before
	public void setUp() throws Exception {
		driver = new ExtractSentenceDriver2();
	}

	@Test
	public void testUDF() {
		String sen1 = "제자리에 머무는 사람에게는 발전이 없다";
		String sen2 = " 이는 좌우명입니다";
		String sen3 = " 어릴때부터 키워왔습니다";
		String sen4 = " 그 꿈을 당연한 결과라고 생각합니다";
		String sen5 = " 대학교에서 맘껏 발휘하고 싶습니다";
		String sen6 = " 그리고 가장 적합하다고 생각하기 때문입니다";
		String sen7 = " 그리고 개발 부문에 지원하게 되었습니다";
		String sen8 = " 작은 시켜보겠습니다";
		String sen9 = " 그리하여 현재 세계시장을 주름잡data고 있는 일조하겠습니다";

		StringBuilder allSentence = new StringBuilder();
		allSentence.append(sen1).append(sen2).append(sen3).append(sen4).append(sen5);
		allSentence.append(sen6).append(sen7).append(sen8).append(sen9);

		// String sen1 = "제자리에 머무는 사람에게는 발전이 없다" + " 이는 좌우명입니다" + " 어릴때부터 키워왔습니다" + " 그 꿈을 당연한 결과라고 생각합니다"
		// + " 대학교에서 맘껏 발휘하고 싶습니다" + " 그리고 가장 적합하다고 생각하기 때문입니다" + " 그리고 개발 부문에 지원하게 되었습니다" + " 작은 시켜보겠습니다"
		// + " 그리하여 현재 세계시장을 주름잡고 있는 일조하겠습니다";
		// String sen1 = "제자리에 머무는 사람에게는 발전이 없다";

		Text expect1 = new Text(sen1);
		Text expect2 = new Text(sen1 + sen2);
		// Text expect3 = new Text(sen1 + sen2 + sen3);
		// Text expect4 = new Text(sen1 + sen2 + sen3 + sen4);
		// Text expect5 = new Text(sen1 + sen2 + sen3 + sen4 + sen5);

		ExtractSentenceDriver2 driver = new ExtractSentenceDriver2();

		// Assert.assertEquals(expect1, driver.evaluate(new Text(sen1), new IntWritable(1)));
		Assert.assertEquals(expect2, driver.evaluate(new Text(sen1), new IntWritable(2)));
		// Assert.assertEquals(expect3, driver.evaluate(new Text(sen1), new IntWritable(3)));
		// Assert.assertEquals(expect4, driver.evaluate(new Text(sen1), new IntWritable(4)));
		// Assert.assertEquals(expect5, driver.evaluate(new Text(sen1), new IntWritable(5)));

		// Assert.assertEquals(expect1, driver.evaluate(new Text(allSentence.toString()), new IntWritable(1)));
	}

	@Test
	public void testOutOfIndexCheck() {

		// String sen1 = "성장하는 연구원 년 간 광소자의 특성평가 분석을 하며 관련 전문지식을 습득했습니다 이를 바탕으로 현재 불량분석을 주로 담당하며 실제 제품이 사용되는 과 등에 관심을 갖게
		// 되었고 신소재 전공자로서 전자재료 뿐 아닌 금속을 아우르는 소재분석 전문가로 발전하고 싶어 지원합니다 이를 위해 누구보다 체험하고 경험하는 실천적인 연구원이 되고자 합니다 제품분석 을 통한 경쟁사
		// 제품의 특성평가 및 자사제품과 비교분석 신기술 동향 파악 개발방향 제시 불량분석 불량 현상 발생 원인 분석 및 특성 향상 위한 소재분석 담당 분석 장비 이외 자료해석 가능 장비 적분구
		// 열화상카메라";
		// Text expect1 = new Text("성장하는 연구원 년 간 광소자의 특성평가 분석을 하며 관련 전문지식을 습득했습니다");
		//
		// Assert.assertEquals(expect1, driver.evaluate(new Text(sen1), new IntWritable(1)));

		String sentence = "0123456789";
		Assert.assertEquals("", driver.evaluate(new Text(sentence), new IntWritable(1)));
	}

	@Test
	public void testIndex() throws Exception {
		String sentence = "0123456789";

		int end = 9;
		if (end <= sentence.length()) {
			System.out.println(sentence.substring(8, 9));
		}
		end = 10;
		if (end <= sentence.length()) {
			System.out.println(sentence.substring(9, 10));
		}
		end = 11;
		if (end <= sentence.length()) {
			System.out.println(sentence.substring(9, 11));
		}
	}

	@Test
	public void testSetence() throws Exception {
		String sen = "다시 태어나도 내 회사 인사 담당자님께 질문 드립니다 다시 태어나셔도 현대자동차에 입사하시겠습니까 저는 다시 태어나도 현대자동차에 입사할 겁니다 제가 너무나 사랑하는 회사니까요 현대 자동차 채용 설명회 당시 제 가슴에 박힌 말입니다 내부 구성원이 이렇듯 온 마음을 다해 헌신하는 회사를 누가 들어가고 싶어하지 않겠습니까 인사 담당자 님의 말씀에 현대자동차의 강점을 또 한 가지 알게 되었습니다 이제 저 또한 당당히 외치고 싶습니다 제 모든 것을 바칠 제 회사 현대 자동차를 사랑한다고 말입니다 세대공감 우리를 제일 잘 이해해주는 선생님 약 년 간 학원 강사로 근무하면서 학부모님과 아이들에게 가장 많이 들었던 말입니다 대부터 이 넘으신 학부모님들까지 다양한 성격의 사람들을 만나 이야기를 듣고 공감하며 년을 보냈습니다 소통이 시대의 화두인 지금 대부터 대까지 다양한 연령대의 사람들을 꾸준히 만나 그들의 삶의 이야기를 들을 수 있는 기회는 제 또래에서는 많이 없다고 생각합니다 현대자동차의 고객도 마찬가지입니다 갓 면허를 딴 대에서 대까지 영업 지원은 년 년 혹은 년 이상이 될 지도 모르는 그들의 자동차와 동행하며 소통하는 자리라고 생각합니다 제가 그 역할의 선봉장이 되겠습니다 의 무대 넌 못하는 게 뭐야 유리야 넌 모르는 게 없는 것 같아 이 세상에는 한 분야에 정통한 수많은 가 있습니다 저는 는 아닙니다 하지만폭 넓은 분야에 대한 지식을 바탕으로 수많은 고객들의 니즈를 파악하고 이를 이용하여 높은 성과를 이루어낼 수 있는 능력 저 최유리가 가지고 있습니다 고객관리부터 후속조치까지 고객의 바로 뒤에서 직접 발로 뛰어야 하는 영업지원이야말로 수많은 들의 무대라고 생각합니다 이 무대에서 제가 가진 역량을 최대한 발휘하겠습니다";

		Text evaluate = driver.evaluate(new Text(sen), new IntWritable(5));
		System.out.println(evaluate.toString());
	}
}
