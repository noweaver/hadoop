package hive.udf;

import java.lang.Character.UnicodeBlock;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

import com.google.common.base.CharMatcher;

public class ExtractKorean extends UDF {
	public Text evaluate(Text column) {
		StringBuilder extractHangul = new StringBuilder(512);
		String data = CharMatcher.JAVA_ISO_CONTROL.removeFrom(column.toString()).replaceAll("\\s+", " ").trim();

		for (int i = 0, size = data.length(); i < size; i++) {
			char ch = data.charAt(i);
			if (ch == ' ' || ch == '.' || ch == '?' || ch == '!') {
				extractHangul.append(ch);
				continue;
			}

			Character.UnicodeBlock unicodeBlock = Character.UnicodeBlock.of(ch);
			if (UnicodeBlock.HANGUL_SYLLABLES.equals(unicodeBlock)
					|| UnicodeBlock.HANGUL_COMPATIBILITY_JAMO.equals(unicodeBlock)
					|| UnicodeBlock.HANGUL_JAMO.equals(unicodeBlock)) {
				extractHangul.append(ch);
			}
		}

		data = extractHangul.toString();
		data = data.replaceAll("[!]+", "!");
		data = data.replaceAll("[.]+", ".");
		data = data.replaceAll("[?]+", "?");		
		data = data.replaceAll("\\s+", " ").trim();
		
		char ch = data.charAt(0);
		if (ch == '.' || ch == '?' || ch == '!') {
			data = data.substring(1, data.length()).replaceAll("\\s+", " ").trim();
		}
		
		return new Text(data);
	}
}
