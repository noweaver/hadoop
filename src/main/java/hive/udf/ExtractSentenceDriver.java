package hive.udf;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

import com.google.common.base.CharMatcher;

public class ExtractSentenceDriver extends UDF {

	public Text evaluate(Text column, IntWritable sentenceNum) {
		String filter[] = { "니다", "없다", "있다", "이다" };

		int sentenceCnt = 0;
		StringBuilder temp = new StringBuilder(2);
		StringBuilder extractSentence = new StringBuilder(256);
		StringBuilder sentenceBunch = new StringBuilder(256);

		String data = CharMatcher.JAVA_ISO_CONTROL.removeFrom(column.toString());
		// same usage following as;
		// column.toString().replaceAll("\\p{Cntrl}", "");
		
		int dataLength = data.length();
		int num = sentenceNum.get();

		int pos = 0, end = 0;
		for (int i = 0; i < dataLength; i++) {
			int index = i + 1;

			if (index < dataLength) {
				char firstCha = data.charAt(i);
				char secondCha = data.charAt(index);
				temp.append(firstCha).append(secondCha);

				String comp = temp.toString();
				temp.setLength(0);
				for (int j = 0, size = filter.length; j < size; j++) {
					end = data.indexOf(comp, pos);
					if (end <= dataLength) {
						if (filter[j].equals(comp)) {
							String setence = data.substring(pos, end + 2);
							pos = end + 2;
							extractSentence.append(setence);
							sentenceCnt++;

							break;
						}
					}
				}

				if (extractSentence.length() > 0) {
					sentenceBunch.append(extractSentence);
					extractSentence.setLength(0);
				}

			}
			if (sentenceCnt == num) {
				break;
			}
		}
		String setences = sentenceBunch.toString();
		sentenceBunch.setLength(0);
		return new Text(setences);
	}
}
