package hive.udf;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class ExtractSentenceDriver2 extends UDF {
	public Text evaluate(Text column, IntWritable num) {
		String filter[] = { "니다", "없다", "있다", "니까", "이다", "까요" };

		String data = column.toString();
		int extractNum = num.get();

		int cnt = 0;
		StringBuilder sentences = new StringBuilder(256);
		for (int i = 0, size = filter.length; i < size; i++) {
			String senEnding = filter[i];
			
			int pos = 0, index = 0;
			String substring = null;
			while ((index = data.indexOf(senEnding, pos)) >= 0) {
				int end = index + 2;
				int dataLength = data.length();

				if (end <= dataLength) {
					substring = data.substring(index, index + 2);
					if (senEnding.equals(substring)) {

						sentences.append(data.substring(pos, index)).append(substring);
						cnt++;
					}
					pos = index + 1;
				}
			}
			if (extractNum == cnt) {
				break;
			}
		}

		System.out.println(sentences.toString());
		return new Text(sentences.toString());
	}
}
