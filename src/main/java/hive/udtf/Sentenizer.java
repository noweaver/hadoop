package hive.udtf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

@Description(name = "sentenizer", value = "_FUNC_(id, contents) - emits (id, sentence, 1) for each sentence in the input document")
public class Sentenizer extends GenericUDTF {

	private PrimitiveObjectInspector idOI;
	private PrimitiveObjectInspector contentOI;

	public StructObjectInspector initialize(ObjectInspector[] args) throws UDFArgumentException {

		if (args.length != 2) {
			throw new UDFArgumentException("sentenizer() takes exactly 2 argument");
		}

		if (args[0].getCategory() != ObjectInspector.Category.PRIMITIVE && ((PrimitiveObjectInspector) args[0])
				.getPrimitiveCategory() != PrimitiveObjectInspector.PrimitiveCategory.STRING) {
			throw new UDFArgumentException("sentenizer() takes a string as a id");
		}

		if (args[1].getCategory() != ObjectInspector.Category.PRIMITIVE && ((PrimitiveObjectInspector) args[1])
				.getPrimitiveCategory() != PrimitiveObjectInspector.PrimitiveCategory.STRING) {
			throw new UDFArgumentException("sentenizer() takes a string as a content");
		}

		idOI = (PrimitiveObjectInspector) args[0];
		contentOI = (PrimitiveObjectInspector) args[1];

		List fieldNames = new ArrayList(2);
		List fieldOIs = new ArrayList(2);

		fieldNames.add("id");
		fieldNames.add("sentence");
		fieldNames.add("cnt");
		fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
		fieldOIs.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
		fieldOIs.add(PrimitiveObjectInspectorFactory.javaIntObjectInspector);
		return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldOIs);

	}

	@Override
	public void process(Object[] record) throws HiveException {

		String id = ((String) idOI.getPrimitiveJavaObject(record[0]));
		String data = ((String) contentOI.getPrimitiveJavaObject(record[1]));

		String match = "[\"\',;:\\/()<>\\[\\]{}]";
		data = data.replaceAll(match, "");
		String match2 = "[  ]+";
		data = data.replaceAll(match2, " ");

		List<String> puncSenList = extractSentences(data, ".");

		List<String> questionSenList = new ArrayList<String>();
		for (String sentence : puncSenList) {
			questionSenList.addAll(extractSentences(sentence, "?"));
		}

		List<String> batSenList = new ArrayList<String>();
		for (String sentence : questionSenList) {
			batSenList.addAll(extractSentences(sentence, "!"));
		}

		List<String> filterSentences = new ArrayList<String>();
		for (String sentence : batSenList) {
			filterSentences.addAll(fiterSentence(sentence));
		}

		Map<String, Integer> tempMap = new LinkedHashMap<String, Integer>();
		Integer value = 0;
		for (String sentence : filterSentences) {
			value = tempMap.get(sentence);
			if (value == null) {
				tempMap.put(sentence, 1);
			} else {
				tempMap.put(sentence, value + 1);
			}
		}

		Iterator<String> it = tempMap.keySet().iterator();
		String sentence = null;
		while (it.hasNext()) {
			sentence = it.next();
			forward(new Object[] { id, sentence, tempMap.get(sentence) });
		}
	}

	private List<String> extractSentences(String data, String sep) {
		List<String> puncSenList = new ArrayList<String>();
		int pos = 0, end = 0;
		while ((end = data.indexOf(sep, pos)) > 0) {
			String temp = data.substring(pos, end).trim();
			if (temp.length() != 0) {
				puncSenList.add(temp);
			}
			pos = end + 1;
		}
		String lastSen = data.substring(pos, data.length()).trim();
		if (!isNullOrBlank(lastSen)) {
			puncSenList.add(lastSen);
		}
		return puncSenList;
	}

	public static boolean isNull(String str) {
		return str == null ? true : false;
	}

	public static boolean isNullOrBlank(String param) {
		if (isNull(param) || param.trim().length() == 0) {
			return true;
		}
		return false;
	}

	@Override
	public void close() throws HiveException {

	}

	private List<String> fiterSentence(String sentence) {
		List<String> sentenceList = new ArrayList<String>();
		String filters[] = { "니다", "이다" };

		String data = sentence.toString();

		for (int i = 0, size = filters.length; i < size; i++) {
			String senEnding = filters[i];

			int pos = 0, index = 0;
			String filter = null;
			while ((index = data.indexOf(senEnding, pos)) >= 0) {
				int end = index + 2;
				int dataLength = data.length();

				if (end <= dataLength) {
					filter = data.substring(index, index + 2);
					if (senEnding.equals(filter)) {

						String sen = data.substring(pos, index + 2).replaceAll("[!]+", "");
						sen = sen.replaceAll("[  ]+", " ").trim();

						if (!isNullOrBlank(sen)) {
							sentenceList.add(sen);
						}

					}
					pos = index + 2;
				}
			}
		}
		return sentenceList;
	}
}
