package similarity.cosine.mr;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CosineSimilarityDriverTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws Exception {
		String[] args = {"/Volumes/Workspace/Git/snippet-hadoop/data/input/part-r-00000"
				, "/Volumes/Workspace/Git/snippet-hadoop/data/output/"
				, "/Volumes/Workspace/Git/snippet-hadoop/data/input/denominator_data"};
		CosineSimilarityDriver.main(args );
	}

}
