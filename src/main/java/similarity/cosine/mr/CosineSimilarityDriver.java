package similarity.cosine.mr;

import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class CosineSimilarityDriver extends Configured implements Tool {
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new CosineSimilarityDriver(), args);
		System.exit(res);

	}

	@Override
	public int run(String[] args) throws Exception {
		if (args.length != 3) {
			String msg = "Usage: hadoop jar cosine-similarity-job.jar"
					+ " [generic options] input output ditributedcahed-path";
			System.err.println(msg);
			System.err.println();
			GenericOptionsParser.printGenericCommandUsage(System.err);
			return 1;
		}

		String INPUT_PATH = args[0];
		String OUTPUT_PATH = args[1];
		String DISTRIBUTEDCAHED_PATH = args[2];

		Configuration conf = getConf();
		conf.set("mapreduce.map.output.compress", "true");
		conf.set("mapreduce.output.fileoutputformat.compress.type", "BLOCK");
		conf.set("mapreduce.map.output.compress.codec", "org.apache.hadoop.io.compress.SnappyCodec");

		// memory tuning
		conf.set("mapreduce.map.memory.mb", "32768");
		conf.set("mapreduce.map.java.opts", "-Xmx26214m");
		conf.set("mapreduce.reduce.memory.mb", "32768");
		conf.set("mapreduce.reduce.java.opts", "-Xmx26214m");
		conf.set("yarn.app.mapreduce.am.resource.mb", "32768");
		conf.set("yarn.app.mapreduce.am.command-opts", "-Xmx26214m");

		// add tuning
		final long HEARTBEAT_TIMEOUT = 1440 * 60 * 1000;
		conf.setLong("mapreduce.task.timeout", HEARTBEAT_TIMEOUT);

		Job job = Job.getInstance(conf, "Cosine Similarity");
		// job.addCacheFile(new URI(DISTRIBUTEDCAHED_PATH));

		DistributedCache.addCacheFile(new Path(DISTRIBUTEDCAHED_PATH).toUri(), job.getConfiguration());

		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(OUTPUT_PATH), true);
		job.setJarByClass(CosineSimilarityDriver.class);

		job.setMapperClass(CosineSimilarityMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(DoubleWritable.class);

		job.setReducerClass(CosineSimilarityReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(DoubleWritable.class);

		FileInputFormat.addInputPath(job, new Path(INPUT_PATH));
		FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH));

		return job.waitForCompletion(true) ? 0 : 1;

	}

}
