package similarity.cosine.mr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CosineParingReducer extends Reducer<Text, Text, Text, DoubleWritable> {

	@Override
	protected void reduce(Text word, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		List<String> docs = new ArrayList<String>();
		Map<String, Double> docTfidfs = new TreeMap<String, Double>();

		String[] items = null;
		String docName = null;
		double tfidf = 0.0;
		Iterator<Text> it = values.iterator();
		// word,doc_id:tfidf
		while (it.hasNext()) {
			items = it.next().toString().split(":");
			docName = items[0];
			tfidf = Double.parseDouble(items[1]);

			docs.add(docName);
			docTfidfs.put(docName, tfidf);
		}

		Collections.sort(docs);

		String doc1 = null;
		Double doc1TfIdf = null;
		String doc2 = null;
		Double doc2TfIdf = null;
		for (int i = 0, size_i = docs.size(); i < size_i - 1; i++) {
			doc1 = docs.get(i);
			doc1TfIdf = docTfidfs.get(doc1);

			for (int j = i + 1, size_j = docs.size(); j < size_j; j++) {
				doc2 = docs.get(j);
				doc2TfIdf = docTfidfs.get(doc2);

				context.write(new Text(doc1 + ":" + doc2), new DoubleWritable(doc1TfIdf * doc2TfIdf));
			}
		}
	}
}
