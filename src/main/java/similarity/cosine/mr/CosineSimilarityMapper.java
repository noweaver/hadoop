package similarity.cosine.mr;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CosineSimilarityMapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {

	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		if (key.get() > 0) {
			String[] values = value.toString().split("\t");
			String twoDocsName = values[0];
			double numerator = Double.parseDouble(values[1]);
			
			context.write(new Text(twoDocsName), new DoubleWritable(numerator));
		}
	}
}
