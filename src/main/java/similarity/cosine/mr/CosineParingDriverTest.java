package similarity.cosine.mr;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

public class CosineParingDriverTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws Exception {
		String[] args = { "/Volumes/Workspace/Git/snippet-hadoop/data/input",
				"/Volumes/Workspace/Git/snippet-hadoop/data/output" };
		CosineParingDriver.main(args);
	}

	@Test
	public void testSort() throws Exception {
		Map<String, Integer> val = new TreeMap<String, Integer>();
		val.put("B", 4);
		val.put("C", 1);
		val.put("A", 3);
		val.put("E", 6);
		val.put("D", 2);

		System.out.println(val.keySet());
	}
	
	@Test
	public void testSort2() throws Exception {
		List<String> val = new ArrayList<String>();
		val.add("B");
		val.add("C");
		val.add("A");
		val.add("E");
		val.add("D");
		
		Collections.sort(val);
		for (String s : val) {
			System.out.println(s);
		}
	}
	
	@Test
	public void testSubstring() throws Exception {
		
		String docName = "2013_regular_first_hmagezone@naver.com";
		String[] splitedName = docName.split("_");
		StringBuffer sb = new StringBuffer();
		sb.append(splitedName[0].substring(splitedName[0].length()-2));
		sb.append(splitedName[1].substring(0,1));
		sb.append(splitedName[2].substring(0,1)).append("_");
		sb.append(splitedName[3]);
		
		for(String s : splitedName){
			System.out.println(s);
		}
		System.out.println(sb.toString());
	}
}
