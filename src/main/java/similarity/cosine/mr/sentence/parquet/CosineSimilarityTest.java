package similarity.cosine.mr.sentence.parquet;

import org.junit.Before;
import org.junit.Test;

public class CosineSimilarityTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws Exception {
		String[] args = { "/Volumes/Workspace/Git/snippet-hadoop/data/parquet"
				, "/Volumes/Workspace/Git/snippet-hadoop/temp/denominator"
				, "/Volumes/Workspace/Git/snippet-hadoop/data/output" };
		CosineSimilarity.main(args);
	}

}
