package similarity.cosine.mr.sentence.parquet;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import parquet.example.data.Group;

public class CosineSimilarityMapper extends Mapper<LongWritable, Group, Text, DoubleWritable> {

	@Override
	protected void map(LongWritable key, Group value, Context context) throws IOException, InterruptedException {
		String[] fields = value.toString().split("\n");

		String[] pairDocs = fields[0].split(":");
		String twoDocs = pairDocs[1].trim() + ":" + pairDocs[2];
		String[] numerator = fields[1].split(":");

		context.write(new Text(twoDocs), new DoubleWritable(Double.parseDouble(numerator[1].trim())));
	}
}
