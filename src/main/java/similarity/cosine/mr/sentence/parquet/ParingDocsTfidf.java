package similarity.cosine.mr.sentence.parquet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import parquet.Log;
import parquet.hadoop.ParquetFileReader;
import parquet.hadoop.example.ExampleInputFormat;
import parquet.hadoop.example.ExampleOutputFormat;
import parquet.hadoop.example.GroupWriteSupport;
import parquet.hadoop.metadata.CompressionCodecName;
import parquet.hadoop.metadata.ParquetMetadata;
import parquet.schema.MessageType;
import parquet.schema.MessageTypeParser;

public class ParingDocsTfidf extends Configured implements Tool {
	private static final Log LOG = Log.getLog(ParingDocsTfidf.class);

	private static String INPUT_PATH;
	private static String OUTPUT_PATH;
	private static String COMPRESSION;

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new ParingDocsTfidf(), args);
		System.exit(res);

	}

	@Override
	public int run(String[] args) throws Exception {
		if (args.length != 2) {
			String msg = "Usage: hadoop jar paring-docs-tfidf-job.jar" + " [generic options] input output";
			System.err.println(msg);
			System.err.println();
			GenericOptionsParser.printGenericCommandUsage(System.err);
			return 1;
		}

		INPUT_PATH = args[0];
		OUTPUT_PATH = args[1];
		COMPRESSION = (args.length > 2) ? args[2] : "none";

		Configuration conf = getConfiguration();
		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(OUTPUT_PATH), true);

		// ----------------------------------------------------------------------------------------------------
		// Find a file in case a directory was passed
		Path parquetFilePath = null;
		RemoteIterator<LocatedFileStatus> it = FileSystem.get(conf).listFiles(new Path(INPUT_PATH), true);
		while (it.hasNext()) {
			FileStatus fileStatus = it.next();
			if (fileStatus.isFile()) {
				parquetFilePath = fileStatus.getPath();
				break;
			}
		}
		if (parquetFilePath == null) {
			LOG.error("No file found for " + INPUT_PATH);
			return 1;
		}
		LOG.info("Getting schema from " + parquetFilePath);
		ParquetMetadata readFooter = ParquetFileReader.readFooter(conf, parquetFilePath);
		MessageType schema = readFooter.getFileMetaData().getSchema();
		LOG.info(schema);
		GroupWriteSupport.setSchema(schema, conf);
		// ----------------------------------------------------------------------------------------------------

		Job job = Job.getInstance(conf, "Pairing two docs with setence's tfidf");
		job.setJarByClass(ParingDocsTfidf.class);

		job.setMapperClass(ParingDocsTfidfMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		job.setReducerClass(ParingDocsTfidfReducer.class);
		job.setReduceSpeculativeExecution(false);

		job.setInputFormatClass(ExampleInputFormat.class);

		ExampleOutputFormat.setSchema(job, MessageTypeParser.parseMessageType(Schema.SENTENCE_PAIRING_DOCS));
		ExampleOutputFormat.setCompression(job, CompressionCodecName.SNAPPY);

		job.setOutputFormatClass(ExampleOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(INPUT_PATH));
		FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH));

		return job.waitForCompletion(true) ? 0 : 1;
	}

	private Configuration getConfiguration() {
		Configuration conf = getConf();
		conf.set("mapreduce.output.textoutputformat.separator", "\t");

		// compression tuning
//		conf.set("mapreduce.map.output.compress", "true");
//		conf.set("mapreduce.output.fileoutputformat.compress.type", "BLOCK");
//		conf.set("mapreduce.map.output.compress.codec", "org.apache.hadoop.io.compress.SnappyCodec");

		// memory tuning
		conf.set("mapreduce.map.memory.mb", "32768");
		conf.set("mapreduce.map.java.opts", "-Xmx26214m");
		conf.set("mapreduce.reduce.memory.mb", "32768");
		conf.set("mapreduce.reduce.java.opts", "-Xmx26214m");
		conf.set("yarn.app.mapreduce.am.resource.mb", "32768");
		conf.set("yarn.app.mapreduce.am.command-opts", "-Xmx26214m");

		// add tuning
		final long HEARTBEAT_TIMEOUT = 1440 * 60 * 1000;
		conf.setLong("mapreduce.task.timeout", HEARTBEAT_TIMEOUT);

		return conf;
	}

}
