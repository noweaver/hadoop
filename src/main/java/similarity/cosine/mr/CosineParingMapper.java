package similarity.cosine.mr;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CosineParingMapper extends Mapper<LongWritable, Text, Text, Text> {

	@Override
	protected void map(LongWritable key, Text row, Context context) throws IOException, InterruptedException {
		if (key.get() > 0) {
			String[] columns = row.toString().split("\t");

			String docName = columns[0];
			String term = columns[1];
			String tfidf = columns[2];

			context.write(new Text(term), new Text(docName + ":" + tfidf));
		}
	}
}
