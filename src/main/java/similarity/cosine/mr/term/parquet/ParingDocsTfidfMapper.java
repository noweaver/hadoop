package similarity.cosine.mr.term.parquet;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import parquet.example.data.Group;

public class ParingDocsTfidfMapper extends Mapper<LongWritable, Group, Text, Text> {
	@Override
	protected void map(LongWritable key, Group value, Context context) throws IOException, InterruptedException {
		String[] fields = value.toString().split("\n");

		String[] docName = fields[0].split(":");
		String[] term = fields[1].split(":");
		String[] tfidf = fields[2].split(":");

		context.write(new Text(term[1].trim()), new Text(docName[1].trim() + ":" + tfidf[1].trim()));
	}
}
