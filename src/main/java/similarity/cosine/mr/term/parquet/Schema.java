package similarity.cosine.mr.term.parquet;

public class Schema {
	public static String SIMILARITY = "message hive_schema {\n" 
								+ "required binary doc_name1 (UTF8);\n"
								+ "required binary doc_name2 (UTF8);\n" 
								+ "required double similarity;\n" 
								+ "}";
 
	public static String PAIRING_DOCS = "message hive_schema {\n" 
								+ "required binary pair_docs (UTF8);\n"
								+ "required double numerator;\n" 
								+ "}";
}
