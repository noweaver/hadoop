package similarity.cosine.mr.term.parquet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;

import parquet.Log;
import parquet.column.page.PageReadStore;
import parquet.example.data.Group;
import parquet.example.data.simple.SimpleGroupFactory;
import parquet.example.data.simple.convert.GroupRecordConverter;
import parquet.hadoop.ParquetFileReader;
import parquet.hadoop.example.GroupWriteSupport;
import parquet.hadoop.metadata.ParquetMetadata;
import parquet.io.ColumnIOFactory;
import parquet.io.MessageColumnIO;
import parquet.io.RecordReader;
import parquet.schema.MessageType;

public class CosineSimilarityUsingParquetOutputReducer extends Reducer<Text, DoubleWritable, Void, Group> {
	private static final Log LOG = Log.getLog(CosineSimilarityUsingParquetOutputReducer.class);
	private Map<String, Double> denominatorMap;
	private SimpleGroupFactory factory;

	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		denominatorMap = new HashMap<String, Double>();
		Configuration conf = context.getConfiguration();
		Path[] cacheFiles = DistributedCache.getLocalCacheFiles(conf);

		if (cacheFiles != null && cacheFiles.length > 0) {
			String filePath = null;
			try {
				Path path = cacheFiles[0];
				filePath = path.toString();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			try {
				String line = null;

				String docName = null;
				double denominator = 0.0;
				while ((line = br.readLine()) != null) {
					int sepIndex = line.indexOf("\t");
					docName = line.substring(0, sepIndex);
					denominator = Double.parseDouble(line.substring(sepIndex + 1));

					denominatorMap.put(docName, denominator);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				br.close();
			}
		}
		
		
		// distributed cached file, which is parquet file, read
		// ------------------------------------------------------------------------------------------------------
		MessageType schema = GroupWriteSupport.getSchema(conf);
		factory = new SimpleGroupFactory(schema);
		// if (null != cacheFiles && cacheFiles.length > 0) {
		// Path cacheFilePath = cacheFiles[0];
		// Path parquetFilePath = null;
		//
		// RemoteIterator<LocatedFileStatus> it = FileSystem.get(conf).listFiles(cacheFilePath, true);
		// while (it.hasNext()) {
		// FileStatus fs = it.next();
		// if (fs.isFile()) {
		// parquetFilePath = fs.getPath();
		// break;
		// }
		// }
		// LOG.info("Getting schema from " + parquetFilePath);
		// ParquetMetadata readFotter = ParquetFileReader.readFooter(conf, parquetFilePath);
		// MessageType cacheSchema = readFotter.getFileMetaData().getSchema();
		//
		// ParquetFileReader parquetFileReader = null;
		// try {
		// parquetFileReader = new ParquetFileReader(conf, parquetFilePath, readFotter.getBlocks(),
		// cacheSchema.getColumns());
		//
		// PageReadStore pages = null;
		// while (null != (pages = parquetFileReader.readNextRowGroup())) {
		// final long rows = pages.getRowCount();
		// LOG.info("Number of rows:" + rows);
		//
		// final MessageColumnIO columnIO = new ColumnIOFactory().getColumnIO(cacheSchema);
		// final RecordReader<Group> recordReader = columnIO.getRecordReader(pages,
		// new GroupRecordConverter(cacheSchema));
		// for (int i = 0; i < rows; i++) {
		// final Group g = recordReader.read();
		// String[] fields = g.toString().split("\n");
		//
		// String docName = fields[0].split(":")[1].trim();
		// double denominator = Double.parseDouble(fields[1].split(":")[1].trim());
		//
		// denominatorMap.put(docName, denominator);
		// }
		// }
		// } finally {
		// parquetFileReader.close();
		// }
		// }
		// ------------------------------------------------------------------------------------------------------
	}

	@Override
	protected void reduce(Text text, Iterable<DoubleWritable> values, Context context)
			throws IOException, InterruptedException {
		String twoDocsName = text.toString();
		int sepIndex = twoDocsName.indexOf(":");
		String doc1Name = twoDocsName.substring(0, sepIndex);
		String doc2Name = twoDocsName.substring(sepIndex + 1);

		double numerator = 0.0;
		for (DoubleWritable value : values) {
			numerator += value.get();
		}

		Double doc1Denominator = denominatorMap.get(doc1Name);
		Double doc2Denominator = denominatorMap.get(doc2Name);

		double cosineSimilarity = numerator / (doc1Denominator * doc2Denominator);

		Group group = factory.newGroup();
		group.append("doc_name1", doc1Name);
		group.append("doc_name2", doc2Name);
		group.append("similarity", cosineSimilarity);

		context.write(null, group);
	}
}
