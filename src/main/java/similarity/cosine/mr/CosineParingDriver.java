package similarity.cosine.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class CosineParingDriver extends Configured implements Tool {
	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new CosineParingDriver(), args);
		System.exit(res);

	}

	private static String INPUT_PATH;// = "/Volumes/Workspace/Git/snippet-hadoop/data/input";
	private static String OUTPUT_PATH;// = "/Volumes/Workspace/Git/snippet-hadoop/data/output";

	@Override
	public int run(String[] args) throws Exception {
		if (args.length != 2) {
			String msg = "Usage: hadoop jar cosine-paring-job.jar.jar" + " [generic options] input output";
			System.err.println(msg);
			System.err.println();
			GenericOptionsParser.printGenericCommandUsage(System.err);
			return 1;
		}

		INPUT_PATH = args[0];
		OUTPUT_PATH = args[1];

		Configuration conf = getConf();
		conf.set("mapreduce.output.textoutputformat.separator", "\t");

		// compression tuning
		conf.set("mapreduce.map.output.compress", "true");
		conf.set("mapreduce.output.fileoutputformat.compress.type", "BLOCK");
		conf.set("mapreduce.map.output.compress.codec", "org.apache.hadoop.io.compress.SnappyCodec");

		// memory tuning
		conf.set("mapreduce.map.memory.mb", "32768");
		conf.set("mapreduce.map.java.opts", "-Xmx26214m");
		conf.set("mapreduce.reduce.memory.mb", "32768");
		conf.set("mapreduce.reduce.java.opts", "-Xmx26214m");
		conf.set("yarn.app.mapreduce.am.resource.mb", "32768");
		conf.set("yarn.app.mapreduce.am.command-opts", "-Xmx26214m");

		// add tuning
		final long HEARTBEAT_TIMEOUT = 1440 * 60 * 1000;
		conf.setLong("mapreduce.task.timeout", HEARTBEAT_TIMEOUT);

		// conf.set("yarn.am.liveness-monitor.expiry-interval-ms", "1800000");
		// conf.set("yarn.nm.liveness-monitor.expiry-interval-ms", "1800000");
		// conf.set("yarn.resourcemanager.nm.liveness-monitor.interval-ms", "1800000");

		// conf.set("mapreduce.reduce.maxattempts", "5");
		// conf.set("mapreduce.task.io.sort.mb", "1024");
		// conf.set("mapreduce.task.io.sort.factor", "128");

		// final long DEFAULT_SPLIT_SIZE = 128 * 1024 * 1024;
		// conf.setLong(FileInputFormat.SPLIT_MAXSIZE,
		// conf.getLong(FileInputFormat.SPLIT_MAXSIZE, DEFAULT_SPLIT_SIZE) / 4);

		Job job = Job.getInstance(conf, "Cosine Pairing");

		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(OUTPUT_PATH), true);
		job.setJarByClass(CosineParingDriver.class);

		job.setMapperClass(CosineParingMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		job.setReducerClass(CosineParingReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(DoubleWritable.class);
		// job.setOutputValueClass(Text.class);
		// job.setNumReduceTasks(5);

		// job.setInputFormatClass(SequenceFileInputFormat.class);
		// job.setOutputFormatClass(SequenceFileOutputFormat.class);

		job.setReduceSpeculativeExecution(false);

		FileInputFormat.addInputPath(job, new Path(INPUT_PATH));
		FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH));

		return job.waitForCompletion(true) ? 0 : 1;

	}

}
