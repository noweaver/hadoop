package similarity.cosine.mr;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CosineSimilarityReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
	private final Logger logger = LoggerFactory.getLogger(CosineSimilarityReducer.class);

	private Map<String, Double> denominatorMap = null;

	@SuppressWarnings("deprecation")
	@Override
	protected void setup(Context context) throws NullPointerException, IOException, InterruptedException {
		denominatorMap = new HashMap<String, Double>();
		Path[] cacheFiles = DistributedCache.getLocalCacheFiles(context.getConfiguration());

		if (cacheFiles != null && cacheFiles.length > 0) {
			String filePath = null;
			try {
				Path path = cacheFiles[0];
				filePath = path.toString();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			try {
				String line = null;

				String docName = null;
				double denominator = 0.0;
				while ((line = br.readLine()) != null) {
					int sepIndex = line.indexOf("\t");
					docName = line.substring(0, sepIndex);
					denominator = Double.parseDouble(line.substring(sepIndex + 1));

					denominatorMap.put(docName, denominator);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				br.close();
			}
		}
	}

	@Override
	protected void reduce(Text text, Iterable<DoubleWritable> values, Context context)
			throws IOException, InterruptedException {
		String twoDocsName = text.toString();
		int sepIndex = twoDocsName.indexOf(":");
		String doc1Name = twoDocsName.substring(0, sepIndex);
		String doc2Name = twoDocsName.substring(sepIndex + 1);

		double numerator = 0.0;
		for (DoubleWritable value : values) {
			numerator += value.get();
		}

		Double doc1Denominator = denominatorMap.get(doc1Name);
		Double doc2Denominator = denominatorMap.get(doc2Name);

		double cosineSimilarity = numerator / (doc1Denominator * doc2Denominator);

		context.write(new Text(doc1Name + "\t" + doc2Name), new DoubleWritable(cosineSimilarity));
	}

}
