package similarity.cosine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;

public class VectorizeReducer extends Reducer<IntWritable, DocumentWritable, Text, Text> {
	@Override
	protected void reduce(IntWritable key, Iterable<DocumentWritable> values, Context context)
			throws IOException, InterruptedException {
		List<DocumentWritable> docs = new ArrayList<DocumentWritable>();

		for (DocumentWritable doc : values) {
			Text docName = doc.getName();
			MapWritable termTfidfMap = doc.geTermTfidfMap();

			MapWritable mw = new MapWritable();
			for (Writable termWritable : termTfidfMap.keySet()) {
				String term = ((Text) termWritable).toString();
				String tfidf = ((Text) termTfidfMap.get(termWritable)).toString();

				mw.put(new Text(term), new Text(tfidf));
			}

			DocumentWritable dw = new DocumentWritable(docName, mw);
			docs.add(dw);
		}

		StringBuilder twoDocs = new StringBuilder(512);

		for (DocumentWritable doc1 : docs) {
			String doc1Name = doc1.getName().toString();

			for (DocumentWritable doc2 : docs) {
				String doc2Name = doc2.getClass().toString();
				if (doc1Name.equalsIgnoreCase(doc2Name)) {
					continue;
				}

				Set<Writable> twoTerms = new HashSet<Writable>();
				twoTerms.addAll(doc1.geTermTfidfMap().keySet());
				twoTerms.addAll(doc2.geTermTfidfMap().keySet());

				StringBuilder vector1 = new StringBuilder(256);
				StringBuilder vector2 = new StringBuilder(256);
				Iterator<Writable> it = twoTerms.iterator();
				while (it.hasNext()) {
					Writable term = it.next();
					vector1.append(doc1.contain(term)).append(",");
					vector2.append(doc2.contain(term)).append(",");
				}

				twoDocs.append(doc1Name).append("@").append(doc2Name);

				context.write(new Text(twoDocs.toString()), new Text(vector1.toString()));
				context.write(new Text(twoDocs.toString()), new Text(vector2.toString()));
			}
		}

	}
}
