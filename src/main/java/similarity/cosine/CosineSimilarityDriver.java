package similarity.cosine;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class CosineSimilarityDriver extends Configured implements Tool {
	private static String INPUT_PATH = "/Volumes/Workspace/Git/snippet-hadoop/data/input";
	private static String OUTPUT_PATH = "/Volumes/Workspace/Git/snippet-hadoop/data/output";

	public int run(String[] args) throws Exception {
		if (args.length != 3) {
			String msg = "Usage: hadoop jar snippet-hadop-0.0.1-SNAPSHOT-job.jar"
					+ " [generic options] input output reduce_number";
			System.err.println(msg);
			System.err.println();
			GenericOptionsParser.printGenericCommandUsage(System.err);
			return 1;
		}

		INPUT_PATH = args[0];
		OUTPUT_PATH = args[1];
		int REDUCE_SIZE = Integer.parseInt(args[2]);

		Configuration conf = getConf();
		conf.set("mapred.child.java.opts", "-Xmx4096m");

		Job job = Job.getInstance(conf, "Cosine Similarity");

		FileSystem fs = FileSystem.get(conf);
		fs.delete(new Path(OUTPUT_PATH), true);
		job.setJarByClass(CosineSimilarityDriver.class);

		job.setMapperClass(CosineSimilarityMapper.class);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(DocumentWritable.class);

		// job.setReducerClass(CosineSimilarityReducer.class);
		job.setReducerClass(VectorizeReducer.class);
		if (REDUCE_SIZE > 0) {
			job.setNumReduceTasks(REDUCE_SIZE);
		}
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(INPUT_PATH));
		FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH));

		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new CosineSimilarityDriver(), args);
		System.exit(res);
	}

}
