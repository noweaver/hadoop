package similarity.cosine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CosineSimilarityMapper extends Mapper<LongWritable, Text, IntWritable, DocumentWritable> {
	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		int pos = 0, end = 0;
		if (key.get() > 0) {
			String line = value.toString();
			pos = line.indexOf("\t");
			String docName = line.substring(0, pos);

			pos += 1;
			String termScorePair = line.substring(pos, line.length());

			pos = 0;
			List<String> termScorePairList = new ArrayList<String>();
			while ((end = termScorePair.indexOf(",", pos)) > 0) {
				termScorePairList.add(termScorePair.substring(pos, end));
				pos = end + 1;
			}

			termScorePairList.add(termScorePair.substring(pos, termScorePair.length()));
			termScorePair = null;

			pos = 0;
			end = 0;
			MapWritable mw = new MapWritable();
			for (String termScore : termScorePairList) {

				while ((end = termScore.indexOf("=", pos)) > 0) {
					String term = termScore.substring(pos, end);
					pos = end + 1;

					String tfidf = termScore.substring(pos, termScore.length());
					mw.put(new Text(term), new Text(tfidf));
				}
			}

			DocumentWritable dw = new DocumentWritable(new Text(docName), mw);
			context.write(new IntWritable(1), dw);
		}
	}
}
