package similarity.cosine;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class DocumentWritable implements Writable {
	private Text name;
	private MapWritable termTfIdfMap;

	public DocumentWritable() {
		this.name = new Text();
		this.termTfIdfMap = new MapWritable();
	}

	public DocumentWritable(Text key, MapWritable mw) {
		this.name = new Text(key.toString());
		this.termTfIdfMap = mw;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.name.readFields(in);
		this.termTfIdfMap.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.name.write(out);
		this.termTfIdfMap.write(out);

	}

	public Text getName() {
		return this.name;
	}

	public MapWritable geTermTfidfMap() {
		return this.termTfIdfMap;
	}

	@Override
	public String toString() {
		return "Document [name=" + name + "]";
	}

	public String contain(Writable term) {
		if (termTfIdfMap.containsKey(term)) {
			String tfidf = ((Text) termTfIdfMap.get(term)).toString();
			return tfidf;
		}
		return "0.0";
	}

}
