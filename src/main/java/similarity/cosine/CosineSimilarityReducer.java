package similarity.cosine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.SortedMapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;

public class CosineSimilarityReducer extends Reducer<IntWritable, DocumentWritable, Text, Text> {
	@Override
	protected void reduce(IntWritable key, Iterable<DocumentWritable> values, Context context)
			throws IOException, InterruptedException {
		List<DocumentWritable> docs = new ArrayList<DocumentWritable>();

		for (DocumentWritable doc : values) {
			Text docName = doc.getName();
			MapWritable termTfidfMap = doc.geTermTfidfMap();

			MapWritable mw = new MapWritable();
			for (Writable termWritable : termTfidfMap.keySet()) {
				String term = ((Text) termWritable).toString();
				String tfidf = ((Text)termTfidfMap.get(termWritable)).toString();

				mw.put(new Text(term), new Text(tfidf));
			}

			DocumentWritable dw = new DocumentWritable(docName, mw);
			docs.add(dw);
		}

		StringBuilder twoDocs = new StringBuilder(512);
		for (DocumentWritable doc1 : docs) {
			String doc1Name = doc1.getName().toString();

			for (DocumentWritable doc2 : docs) {
				String doc2Name = doc2.getName().toString();

				if (doc1Name.equalsIgnoreCase(doc2Name)) {
					continue;
				}

				MapWritable doc1TermTfidfMap = doc1.geTermTfidfMap();
				MapWritable doc2TermTfidfMap = doc2.geTermTfidfMap();
				MapWritable tempMap = new MapWritable(doc1TermTfidfMap);

				tempMap.keySet().retainAll(doc2TermTfidfMap.keySet());
				int size = doc1TermTfidfMap.size() + doc2TermTfidfMap.size() - tempMap.size();

				double[] doc1Vectors = new double[size];
				double[] doc2Vectors = new double[size];

				// Vectorize
				Set<Writable> terms = new HashSet<Writable>(doc1TermTfidfMap.keySet());
				terms.addAll(doc2TermTfidfMap.keySet());
				int i = 0, j = 0;
				for (Writable termWritalbe : terms) {
					if (doc1TermTfidfMap.containsKey(termWritalbe)) {
						doc1Vectors[i++] = ((DoubleWritable) doc1TermTfidfMap.get(termWritalbe)).get();
					} else {
						doc1Vectors[i++] = 0.0;
					}

					if (doc2TermTfidfMap.containsKey(termWritalbe)) {
						doc2Vectors[j++] = ((DoubleWritable) doc2TermTfidfMap.get(termWritalbe)).get();
					} else {
						doc2Vectors[j++] = 0.0;
					}
				}
			}
		}
	}
}
