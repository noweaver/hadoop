package similarity.cosine;

import org.junit.Before;
import org.junit.Test;

public class CosineSimilarityDriverTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testMain() throws Exception {
		String INPUT_PATH = "/Volumes/Workspace/Git/snippet-hadoop/data/input";
		String OUTPUT_PATH = "/Volumes/Workspace/Git/snippet-hadoop/data/output";
		
		String[] args = {INPUT_PATH, OUTPUT_PATH};
		CosineSimilarityDriver.main(args);
	}
}
