package similarity.tf;

public class Schema {
	public static String TOKENIZED_TF = "message hive_schema {\n" 
								+ "required binary doc_name (UTF8);\n"
								+ "required binary term (UTF8);\n"
								+ "required int32 wc;\n" 
								+ "required double tf;\n" 
								+ "}";
 
}
