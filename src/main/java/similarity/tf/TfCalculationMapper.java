package similarity.tf;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.snu.ids.ha.index.Keyword;
import org.snu.ids.ha.index.KeywordExtractor;
import org.snu.ids.ha.index.KeywordList;

import parquet.example.data.Group;
import parquet.example.data.simple.SimpleGroupFactory;
import parquet.hadoop.example.GroupWriteSupport;
import parquet.schema.MessageType;

//public class TfCalculationMapper extends Mapper<LongWritable, Group, Text, Text> {
public class TfCalculationMapper extends Mapper<LongWritable, Group, Void, Group> {
	private SimpleGroupFactory factory;
	private KeywordExtractor ke;

	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		MessageType schema = GroupWriteSupport.getSchema(context.getConfiguration());
		factory = new SimpleGroupFactory(schema);
		ke = new KeywordExtractor();
	}

	@Override
	protected void map(LongWritable key, Group value, Context context) throws IOException, InterruptedException {
		String[] fields = value.toString().split("\n");

		String doc_name = fields[0].split(":")[1].trim();
		String content = fields[3].split(":")[1].trim();

		KeywordList kl = ke.extractKeyword(content, true);

		Group group = null;
		String term = null;
		int wc = 0;
		double tf = 0.0;
		for (Keyword keyword : kl) {
			term = keyword.getString();
			if (term.length() < 2) {
				continue;
			}

			wc = keyword.getCnt();
			tf = keyword.getFreq();

			group = factory.newGroup();
			group.append("doc_name", doc_name);
			group.append("term", term);
			group.append("wc", wc);
			group.append("tf", tf);

			context.write(null, group);
			// context.write(new Text(doc_name), new Text(term + "\t" + wc + "\t" + tf));
		}
	}

	@Override
	protected void cleanup(Context context) throws IOException, InterruptedException {
		ke = null;
	}
}
