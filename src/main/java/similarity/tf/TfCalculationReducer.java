package similarity.tf;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.snu.ids.ha.index.Keyword;
import org.snu.ids.ha.index.KeywordExtractor;
import org.snu.ids.ha.index.KeywordList;

public class TfCalculationReducer extends Reducer<Text, Text, Text, Text> {
	private KeywordExtractor ke;

	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		ke = new KeywordExtractor();
	}

	@Override
	protected void reduce(Text key, Iterable<Text> contents, Context context) throws IOException, InterruptedException {
		KeywordList kl = null;
		Map<String, Double[]> terms = new TreeMap<String, Double[]>();
		for (Text text : contents) {
			String content = text.toString();
			kl = ke.extractKeyword(content, true);

			for (Keyword keyword : kl) {
				String term = keyword.getString();
				if (term.length() < 2) {
					continue;
				}

				double wc = keyword.getCnt();
				double tf = keyword.getFreq();
				Double[] values = new Double[] { wc, tf };
				terms.put(term, values);
			}
		}

		for (String term : terms.keySet()) {
			Double[] values = terms.get(term);
			context.write(key, new Text(term + "\t" + values[0] + "\t" + values[1]));
		}

	}
}
