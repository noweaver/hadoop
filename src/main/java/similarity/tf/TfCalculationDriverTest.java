package similarity.tf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TfCalculationDriverTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCalculateTf() throws Exception {
		String[] args = { "/Volumes/Workspace/Git/snippet-hadoop/data/input"
				, "/Volumes/Workspace/Git/snippet-hadoop/data/output" };
		TfCalculationDriver.main(args);
	}

	@Test
	public void test() throws IOException {
		BufferedReader br = null;
		try {
			FileReader in = new FileReader(new File("D:/Workspace/Git/snippet-hadoop/data/input/self_intro_0.tsv"));
			br = new BufferedReader(in);

			StringBuilder docName = new StringBuilder(32);
			StringBuilder content = new StringBuilder(1024);

			String line = null;
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] splitedLines = line.split("\t", -1);

				docName.append(splitedLines[0]).append("_");
				docName.append(splitedLines[1]).append("_");
				docName.append(splitedLines[2]).append("_");
				docName.append(splitedLines[4]);

				content.append(splitedLines[6]).append(" ");
				content.append(splitedLines[8]).append(" ");
				content.append(splitedLines[10]).append(" ");
				content.append(splitedLines[12]).append(" ");
				content.append(splitedLines[14]).append(" ");
			}
			System.out.println(docName.toString());
			System.out.println(content.toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}
	}

	@Test
	public void test2() throws Exception {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		// list.add(6);
		// list.add(7);
		// list.add(8);
		// list.add(9);
		// list.add(10);

		// for (int i = 0, size_i = list.size(); i < size_i - 1; i++) {
		// for (int j = i + 1, size_j = list.size(); j < size_j; j++) {
		// System.out.println(i + ":" + j);
		// }
		// }
		// System.out.println("###################");
		//
		// for (int i = list.size(); i >= 0; i--) {
		// for (int j = list.size() - 1; j >= 1; j--) {
		// System.out.println(i + ":" + j);
		// }
		// }
		System.out.println("###################");

		for (int i = 0, size_i = list.size(); i < size_i; i++) {
			for (int j = 0, size_j = list.size(); j < size_j; j++) {
				if (i != j) {
					System.out.println(list.get(i) + ":" + list.get(j));
				}
			}
		}
		System.out.println("###################");

		for (int i = list.size() - 1; i >= 0; i--) {
			for (int j = list.size() - 1; j >= 0; j--) {
				if (i != j) {
					System.out.println(list.get(i) + ":" + list.get(j));
				}
			}
		}
	}
}
