//
// SQL-MR Starter Code Generated by Aster Developer Express V2.0
//

package cos_similarity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;

import com.asterdata.ncluster.sqlmr.ArgumentClause;
import com.asterdata.ncluster.sqlmr.ClientVisibleException;
import com.asterdata.ncluster.sqlmr.Drainable;
import com.asterdata.ncluster.sqlmr.FunctionKind;
import com.asterdata.ncluster.sqlmr.IllegalUsageException;
import com.asterdata.ncluster.sqlmr.InputInfo;
import com.asterdata.ncluster.sqlmr.OutputInfo;
import com.asterdata.ncluster.sqlmr.MultipleInputFunction;
import com.asterdata.ncluster.sqlmr.PartitionFunction;
import com.asterdata.ncluster.sqlmr.RowFunction;
import com.asterdata.ncluster.sqlmr.RuntimeContract;
import com.asterdata.ncluster.sqlmr.data.ColumnDefinition;
import com.asterdata.ncluster.sqlmr.data.MultipleInputs;
import com.asterdata.ncluster.sqlmr.data.PartitionDefinition;
import com.asterdata.ncluster.sqlmr.data.RowEmitter;
import com.asterdata.ncluster.sqlmr.data.RowIterator;
import com.asterdata.ncluster.sqlmr.data.SqlType;

public final class cos_similarity implements MultipleInputFunction {

	//
	// The constructor establishes the RuntimeContract between
	// the SQL-MR function and Aster Database. During query planning,
	// the function will constructed on a single node. During
	// query execution, it will be constructed and run on
	// one or more nodes.
	//
	private final String ITEM_L2NORM="item_l2norm";
	private final String PAIRS_INPUT="pairs_input";

	public cos_similarity(RuntimeContract contract) {

		//
		//Verify that the function accepts the given input schema.
		//
		//1. Generate an array of input names
		String[] inputNames = { ITEM_L2NORM, PAIRS_INPUT };
		if (inputNames.length != contract.getInputCount()) {
			throw new IllegalUsageException(
					"Actual Input Count doesn't exactly match expected Input Count 2!");
		}
		//2. For each input name, put Partition Types info into this map.
		HashMap<String, String> partitionTypes = new HashMap<String, String>();
		partitionTypes.put(ITEM_L2NORM, "DIMENSION");
		partitionTypes.put(PAIRS_INPUT, "PARTITION BY <key>");

		//For each input name specified by the user in the wizard
		for (int i = 0; i < inputNames.length; i++) {
			//
			// Verify InputKind Type: 
			//
			// The input names.
			String inputName = inputNames[i];
			// fetch expected partition type for this input name
			String expectedPartitionTypeForInputName = partitionTypes
					.get(inputName);

			String actualPartitionTypeForInputName = contract
					.getInputInfo(inputName).getInputKind().asText();

			if (actualPartitionTypeForInputName == null
					|| !actualPartitionTypeForInputName
							.equals(expectedPartitionTypeForInputName)) {
				throw new IllegalUsageException(
						"Expected input partition types: ("+ITEM_L2NORM+"=Dimension, "+PAIRS_INPUT+"=PartitionByKey)");
			}

		}
		//
		//Construct the output schema.
		//
		List<ColumnDefinition> outputColumns = new ArrayList<ColumnDefinition>();
		//
		//this for-loop adds all input columns from each input name into the output column definition for multiple input
		//
		//
		//Add additional columns
		//
		outputColumns.add(new ColumnDefinition("cos_item1", SqlType
				.getType("character varying")));
		outputColumns.add(new ColumnDefinition("cos_item2", SqlType
				.getType("character varying")));
		outputColumns.add(new ColumnDefinition("cos_score", SqlType
				.getType("double precision")));
		contract.setOutputInfo(new OutputInfo(outputColumns));

		//
		// Add any other function-specific initialization of member variables
		//   or other checks here. Throw a ClientVisibleException to signal
		//   an error to the client.
		//

		//
		// Complete the contract
		//
		contract.complete();
	}

	public void operateOnMultipleInputs(MultipleInputs multipleInputs,
			RowEmitter outputEmitter) {
		String cos_pair=multipleInputs.getPartitionDefinition().getStringAt(0);
		int sep=cos_pair.indexOf(":");
		String item1=cos_pair.substring(0,sep);
		String item2=cos_pair.substring(sep+1);
		Double item1_l2norm=0.0, item2_l2norm=0.0;
		
		RowIterator inputIterator=multipleInputs.getRowIterator(ITEM_L2NORM);
		while(inputIterator.advanceToNextRow()) {
			String item=inputIterator.getStringAt(0);
			if(item.equals(item1)) {
				item1_l2norm=inputIterator.getDoubleAt(1);
			} else if(item.equals(item2)) {
				item2_l2norm=inputIterator.getDoubleAt(1);
			}
		}

		inputIterator=multipleInputs.getRowIterator(PAIRS_INPUT);
		Double total_ab=0.0;
		while(inputIterator.advanceToNextRow()){
			total_ab+=inputIterator.getDoubleAt(1);
		}
		
		outputEmitter.addString(item1);
		outputEmitter.addString(item2);
		outputEmitter.addDouble(total_ab/item1_l2norm/item2_l2norm);
		outputEmitter.emitRow();
	}
}