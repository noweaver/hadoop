drop table if exists tf_idf;
create table tf_idf() distribute by hash(docid) as
select id as docid, word, tfidf_score
from tf_idf(
   ON (select "Article ID" as docid,regexp_split_to_table("ocr text",'\\\s+') as word from demo_text) AS DATA 
   PARTITION BY word
   ON (select count(distinct "Article ID") as total_no_of_docs from demo_text) AS TOTALS
   DIMENSION
   );
analyze tf_idf;
select count(*) from tf_idf;

-- 1 분자(공통 워딩)
drop table if exists cos_pairs_input;
create table cos_pairs_input() distribute by hash(cos_pair) as
select cos_pair, ab as ab_val
from cos_pair_generator(
   ON (select docid, tfidf_score, word from tf_idf)
   PARTITION BY word
) T;
analyze cos_pairs_input;
select count(*) from cos_pairs_input;

-- 2
drop table if exists doc_similarity_scores;
create table doc_similarity_scores() distribute by hash(cos_item1) as

select cos_item1, cos_item2, cos_score
from cos_similarity(
   ON ( select docid, sqrt(sum(tfidf_score*tfidf_score)) as l2norm
		from tf_idf
		group by 1) as ITEM_L2NORM
	DIMENSION
   ON (select cos_pair, ab_val from cos_pairs_input ) as PAIRS_INPUT
    PARTITION BY cos_pair
) T
;


analyze doc_similarity_scores;
select count(*) from doc_similarity_scores;

select *
from GraphGen(
   ON (select * from doc_similarity_scores where cos_score > 0.1)
   PARTITION BY 1
   ORDER BY cos_score desc
   item_format('individual')
   item1_col('cos_item1')
   item2_col('cos_item2')
   score_col('cos_score')
   domain('192.168.241.100')
   output_format('sigma') directed('false') 
   title('Document similarity')
   );